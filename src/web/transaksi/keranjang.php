<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/web/keranjang/save', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();

  //Save ke alamat jika belum ada di keranjang
  $CountKeranjang = 0;
  $querySelect = $this->db->prepare('SELECT count(*) AS jml FROM keranjang WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
  $querySelect->bindParam(':customer_seq', $dataPost['id_customer']);  
  $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);  
  $result = $querySelect->execute(); 
  if ($result) {
    if ($querySelect->rowCount()) {
        $data = $querySelect->fetch();        
        $CountKeranjang = $data["jml"];
    }
  }  

  if ($CountKeranjang <= 0) {  
    $query = $this->db->prepare('DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
    $query->bindParam(':customer_seq', $dataPost['id_customer']);
    $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
    $result = $query->execute(); 

    $alamat_kirim = "";  
    $query = $this->db->prepare("SELECT nama, alamat, kota, provinsi, no_telp FROM master_customer WHERE seq = :customer_seq");		
    $query->bindParam(':customer_seq', $dataPost['id_customer']);
    $result = $query->execute();

    if ($result) {
      if ($query->rowCount()) {
          $data = $query->fetch();               

          $alamatTmp = "";
          if ($data["alamat"] <> ""){
            $alamatTmp .= $data["alamat"].", ";
          }

          if ($data["kota"] <> ""){
            $alamatTmp .= $data["kota"].", ";
          }

          if ($data["provinsi"] <> ""){
            $alamatTmp .= $data["provinsi"];
          }          

          $alamat_kirim .="Kepada\n";
          $alamat_kirim .="Ykh : ".$data["nama"]."\n";
          $alamat_kirim .= "Alamat : $alamatTmp \n";
          $alamat_kirim .= "No. HP : ".$data["no_telp"];          
      }
    }
    
    $alamat_pengirim ="Gudang Ethica Group"."\n".
                      "BizPark Commercial Estate"."\n\n".    
                      "Jl.Kopo Cirangrang BizPark Blok B3 No.2 dan 6"."\n".
                      "Bandung 40227 - Jawa Barat"."\n\n".    
                      "No. HP : ";

    $query = $this->db->prepare("INSERT INTO keranjang_master (customer_seq, alamat_kirim, tipe_customer, alamat_pengirim, tipe_aplikasi)  
                                VALUES (:customer_seq, :alamat_kirim, :tipe_customer, :alamat_pengirim, 'W')");
    $query->bindParam(':customer_seq', $dataPost['id_customer']);
    $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
    $query->bindParam(':alamat_kirim', $alamat_kirim);    
    $query->bindParam(':alamat_pengirim', $alamat_pengirim);        
    $result = $query->execute();   
  }

  //Ambil qty yang sudah tersimpan
  $querySelect = $this->db->prepare('SELECT qty FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');  
  $querySelect->bindParam(':customer_seq', $dataPost['id_customer']);
  $querySelect->bindParam(':barang_seq', $dataPost['id_barang']);
  $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);
  $result = $querySelect->execute(); 
  $rowCount = $querySelect->rowCount();   

  if ($rowCount > 0) {  
    $hasil = $querySelect->fetch();
    $qty = $hasil["qty"];
  }else{
    $qty = 0;
  }
  
  //Cek stok barang
  $qryCekStok = $this->db->prepare("SELECT stok FROM stok_barang WHERE barang_seq = :barang_seq");  
  $qryCekStok->bindParam(':barang_seq', $dataPost['id_barang']);  
  $result = $qryCekStok->execute(); 
  $rowCount = $qryCekStok->rowCount();     
  
  if ($rowCount > 0) {  
    $hasil = $qryCekStok->fetch();
    if (($qty + $dataPost['qty']) > ($hasil["stok"])){
      return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
    }else{

      //Cek qty yang sudah tersimpan  
      $querySelect = $this->db->prepare('SELECT seq FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');      
      $querySelect->bindParam(':customer_seq', $dataPost['id_customer']);
      $querySelect->bindParam(':barang_seq', $dataPost['id_barang']);
      $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);
      $result = $querySelect->execute(); 
      $rowCount = $querySelect->rowCount(); 

      //Jika ada update jika tidak ada insert baru
      if ($rowCount > 0) {
        $query = $this->db->prepare('UPDATE keranjang SET qty = qty + :qty WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer');        
        $query->bindParam(':customer_seq', $dataPost['id_customer']);
        $query->bindParam(':barang_seq', $dataPost['id_barang']);
        $query->bindParam(':qty', $dataPost['qty']);
        $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);
        $result = $query->execute();
        if($result)
          return $response->withJson(["status" => "success Update", "data" => "1"], 200); 
      }else{
        $query = $this->db->prepare("insert into keranjang (customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi) values 
                                    (:customer_seq, :barang_seq, :qty, :tipe_customer, 'W')");        
        $query->bindParam(':customer_seq', $dataPost['id_customer']);
        $query->bindParam(':barang_seq', $dataPost['id_barang']);
        $query->bindParam(':qty', $dataPost['qty']);
        $query->bindParam(':tipe_customer', $dataPost['tipe_customer']);        
        $result = $query->execute();
        if($result)
          return $response->withJson(["status" => "success Save", "data" => $rowCount], 200);       
      }  
    }       
  }else{    
    return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
  }  
})->add($cekAPIKeyWeb);

$app->get("/web/keranjang/delete/{id}", function (Request $request, Response $response, $args){
    $id = $args["id"];
    $sql = "DELETE FROM keranjang WHERE seq =:id";
    $query = $this->db->prepare($sql);
		$query->bindParam(':id', $args['id']);
    if($query->execute())
       return $response->withJson(["status" => "success", "data" => "1"], 200);

    return $response->withJson(["status" => "failed", "data" => "0"], 200);
})->add($cekAPIKeyWeb);


$app->get('/web/keranjang/load/{id}', function (Request $request, Response $response, array $args) {
  
  $filter = "";
  $tipe_cust   = $request->getQueryParam("tipe_customer");  
  
  $fielddiskon = "(c.diskon_ethica * b.harga)/100";
  if (!empty($tipe_cust)){
    if ($tipe_cust == "E"){ 
      //$filter .= " AND b.brand <> 'SEPLY'";
      $filter .= " AND c.tipe_customer = '$tipe_cust'";
    }
    if ($tipe_cust == "S"){ 
      //$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI')";
      $filter .= " AND c.tipe_customer = '$tipe_cust'";
      $fielddiskon = "(c.diskon_seply * b.harga)/100";
    }
  }

  $query = $this->db->prepare("SELECT b.seq as idbarang, b.barcode as kode, b.nama as nama, b.harga as harga, b.gambar as gambar_tumbnail, b.gambar_besar, b.gambar_sedang, k.seq as id, 
                               k.qty as qty, $fielddiskon as diskon 
                               FROM master_barang b, keranjang k, master_customer c, stok_barang s 
                               where s.barang_seq = b.seq and c.seq = k.customer_seq and k.barang_seq = b.seq and k.customer_seq = :id $filter");
  $query->bindParam(':id', $args['id']);
  $result = $query->execute();
  if ($result) {
    if ($query->rowCount()) {
      $data = $query->fetchAll();
    }else{
      $data = array(
        'kode' => 200,
        'keterangan' => 'Tidak ada data',
        'data' => null);
    }
  }else{
    $data = array(
      'kode' => 100,
      'keterangan' => 'Terdapat error',
      'data' => null);
  }
    return $response->withJson($data);
});

$app->post('/web/keranjang/update_qty', function (Request $request, Response $response) {
  $dataPost = $request->getParsedBody();
  $qryCekStok = $this->db->prepare("SELECT stok FROM stok_barang WHERE barang_seq = :barang_seq");  
  $qryCekStok->bindParam(':barang_seq', $dataPost['id_barang']);
  $result = $qryCekStok->execute(); 
  $rowCount = $qryCekStok->rowCount(); 
  
  if ($rowCount > 0) {  
    $hasil = $qryCekStok->fetch();
    if (($qty + $dataPost['qty']) > ($hasil["stok"] )){
      return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
    }else{
      $query = $this->db->prepare('update keranjang set qty = :qty where seq = :seq');
      $query->bindParam(':seq', $dataPost['id']);
      $query->bindParam(':qty', $dataPost['qty']);
      $result = $query->execute();
      if($result)
        return $response->withJson(["status" => "success", "data" => "1"], 200);  
    }
  }else{
    return $response->withJson(["status" => "stok kurang", "data" => "1"], 200);   
  }   
})->add($cekAPIKeyWeb);