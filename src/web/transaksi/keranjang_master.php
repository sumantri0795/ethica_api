<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/web/keranjang_master/update_alamat_kirim', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();

    $customer_seq  = $dataPost['id_customer']; 
    $alamat_kirim  = $dataPost['alamat_kirim']; 
    $tipe_customer = $dataPost['tipe_customer'];    
    
    $query = $this->db->prepare('UPDATE keranjang_master SET alamat_kirim = :alamat_kirim WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':alamat_kirim', $alamat_kirim);
    $query->bindParam(':tipe_customer', $tipe_customer);
    $result = $query->execute();   

    if($result)
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
})->add($cekAPIKeyWeb);

$app->post('/web/keranjang_master/update_alamat_pengirim', function (Request $request, Response $response) {
    $dataPost = $request->getParsedBody();

    $customer_seq  = $dataPost['id_customer']; 
    $alamat_pengirim  = $dataPost['alamat_pengirim']; 
    $tipe_customer = $dataPost['tipe_customer'];    
    
    $query = $this->db->prepare('UPDATE keranjang_master SET alamat_pengirim = :alamat_pengirim WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
    $query->bindParam(':customer_seq', $customer_seq);
    $query->bindParam(':alamat_pengirim', $alamat_pengirim);
    $query->bindParam(':tipe_customer', $tipe_customer);
    $result = $query->execute();   

    if($result)
        return $response->withJson(["status" => "success", "data" => "1"], 200);      
})->add($cekAPIKeyWeb);

$app->get('/web/keranjang_master/get/{id_customer}/{tipe_customer}', function (Request $request, Response $response, array $args) {
    $query = $this->db->prepare('SELECT customer_seq, alamat_kirim, alamat_pengirim FROM keranjang_master where customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
    $query->bindParam(':customer_seq', $args['id_customer']);
    $query->bindParam(':tipe_customer', $args['tipe_customer']);
      $result = $query->execute();
      if ($result) {
          if ($query->rowCount()) {
              $data = $query->fetchAll();
          }else{
              $data = array(
                  'kode' => 200,
                  'keterangan' => 'Tidak ada data',
                  'data' => null);
          }
      }else{
          $data = array(
              'kode' => 100,
              'keterangan' => 'Terdapat error',
              'data' => null);
      }
      return $response->withJson($data);
  });