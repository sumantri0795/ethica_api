<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/web/pesanan/save', function (Request $request, Response $response) {

	$dataPost = $request->getParsedBody();

	$diskonEthica = 0;
	$diskonSeply  = 0;

	$tipe_cust    = $dataPost['tipe_customer'];  
	$customer_seq = $dataPost['id_customer'];
	
	if (empty($tipe_cust)){
		return $response->withJson(["status" => "error customer", "seq" => 0], 100);   		
	}

	if ($tipe_cust == ""){
		return $response->withJson(["status" => "error customer", "seq" => 0], 100);  				
	}

	if (empty($customer_seq)){
		return $response->withJson(["status" => "error customer", "seq" => 0], 100);
	}

	if ($customer_seq == 0){
		return $response->withJson(["status" => "error customer", "seq" => 0], 100);
	} 

	$query = $this->db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");			
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetch();
			if (!empty($data["is_on"])){
				if ($data["is_on"] == 'F'){
					return $response->withJson(["status" => "status off", "seq" => 0], 100);
				}					
			}						
		}
	}

	$query = $this->db->prepare("SELECT diskon_ethica, diskon_seply FROM master_customer WHERE seq = :seq");		
	$query->bindParam(':seq', $dataPost['id_customer']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetch();
			
			if (!empty($data["diskon_ethica"])){
				$diskonEthica =  $data["diskon_ethica"];
			}else{
				$diskonEthica = 0;
			}			

			if (!empty($data["diskon_seply"])){
				$diskonSeply  =  $data["diskon_seply"];
			}else{
				$diskonSeply  = 0;
			}	
		}
	}

  $query = $this->db->prepare("insert into pesanan_master (tanggal, nomor, customer_seq, total, status, tipe_customer, is_kirim, subtotal, diskon, diskon_ethica, diskon_seply, alamat_kirim, alamat_pengirim, tipe_aplikasi) 
  	values(now(), (select CONCAT('SO-A/', DATE_FORMAT(now(), '%m%y'), '/', lpad((IFNULL(max(substr(nomor,length(nomor)-3, 4)),0)+1),4,'0')) FROM pesanan_master sm 
			where DATE_FORMAT(tanggal, '%m%y') = DATE_FORMAT(now(), '%m%y')), :customer_seq, :total, 'B', :tipe_cust, 'F', :subtotal, :diskon, $diskonEthica, $diskonSeply, :alamat_kirim, :alamat_pengirim, 'W')");
	$query->bindParam(':total', $dataPost['total']);
	$query->bindParam(':customer_seq', $dataPost['id_customer']);
	$query->bindParam(':tipe_cust', $dataPost['tipe_customer']);
	$query->bindParam(':diskon', $dataPost['diskon']);
	$query->bindParam(':subtotal', $dataPost['subtotal']);	
	$query->bindParam(':alamat_kirim', $dataPost['alamat_kirim']);	
	$query->bindParam(':alamat_pengirim', $dataPost['alamat_pengirim']);		

	$result = $query->execute();
	//$id = $this->db->lastInsertId();

	$id = 0;
	$querySelect = $this->db->prepare('SELECT MAX(seq) AS seq FROM pesanan_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer');
  $querySelect->bindParam(':customer_seq', $dataPost['id_customer']);  
  $querySelect->bindParam(':tipe_customer', $dataPost['tipe_customer']);  
  $result = $querySelect->execute();
  if ($result) {
    if ($querySelect->rowCount()) {
				$data = $querySelect->fetch();        				
				$id = $data["seq"];        
    }
  }


  $filter = "";
  $tipe_cust   = $dataPost['tipe_customer'];   

  $fielddiskon = "";
  if (!empty($tipe_cust)){
    if ($tipe_cust == "E"){ 
			//$filter .= " AND b.brand <> 'SEPLY'";
			$fielddiskon = "((c.diskon_ethica * b.harga)/100)";
    }
    if ($tipe_cust == "S"){ 
      //$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI')";
      $fielddiskon = "((c.diskon_seply * b.harga)/100)";
    }
  }

  $query2 = $this->db->prepare("INSERT into pesanan_detail (master_seq, barang_seq, qty, harga, total, is_kirim, diskon, subtotal, tipe_aplikasi) 
																select $id, k.barang_seq, k.qty, b.harga, (b.harga-$fielddiskon) * k.qty, 'F', $fielddiskon, (b.harga) * k.qty, k.tipe_aplikasi 
																from keranjang k, master_barang b, master_customer c 
																where b.seq = k.barang_seq and k.qty > 0 and k.customer_seq = c.seq and k.customer_seq = :customer_seq $filter and k.tipe_customer = '$tipe_cust'");
  $query2->bindParam(':customer_seq', $dataPost['id_customer']);
	$result = $query2->execute(); 

	$query3 = $this->db->prepare("delete from keranjang where customer_seq = :customer_seq and tipe_customer = :tipe_customer and barang_seq in(select seq from master_barang b where seq <> 0 $filter)");	
	$query3->bindParam(':customer_seq', $dataPost['id_customer']);
	$query3->bindParam(':tipe_customer', $dataPost['tipe_customer']);
	$result = $query3->execute();

	$query4 = $this->db->prepare("DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer");
	$query4->bindParam(':customer_seq', $dataPost['id_customer']);
	$query4->bindParam(':tipe_customer', $dataPost['tipe_customer']);	
	$result = $query4->execute();

  return $response->withJson(["status" => "success", "id" => $id], 200);   
})->add($cekAPIKeyWeb);

$app->get('/web/pesanan/get/{id}', function (Request $request, Response $response, array $args) {
  $query = $this->db->prepare("SELECT m.seq as id, m.tanggal, m.nomor, m.customer_seq, m.total as totalmst, m.status, b.seq as id_barang, b.barcode as kode, b.nama as nama, 
														   b.gambar as gambar_tumbnail, b.gambar_sedang, b.gambar_besar, d.harga as harga, d.seq as id_detail,  d.qty as qty, d.total as total_detail, 
															 d.diskon as diskon, m.diskon as diskon_master, m.subtotal as subtotal_master, m.alamat_kirim, m.alamat_pengirim
															 FROM pesanan_master m, pesanan_detail d, master_barang b 
															 where m.seq = d.master_seq and b.seq = d.barang_seq and m.seq = :id");
  $query->bindParam(':id', $args['id']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});

$app->get('/web/pesanan/load_percustomer', function (Request $request, Response $response, array $args) {

	$customer_seq = $request->getQueryParam("id_customer");
	$tipe_cust    = $request->getQueryParam("tipe_customer");
	$offset       = $request->getQueryParam("offset");
	$limit        = $request->getQueryParam("limit");
	
	if (empty($offset)){
		$offset = 0;
	}
	$filter = "";
	if (!empty($status) && ($status != "")){
		$filter .= " AND m.status = '$status'";
	}
	$limitBy = "";
	if (!empty($limit)){
		$limitBy .= "limit $limit OFFSET $offset";
	}
	
  $query = $this->db->prepare("SELECT m.seq as seq, m.tanggal, m.nomor, m.customer_seq as id_customer, m.total as total, m.diskon, m.subtotal, m.status, m.alamat_kirim, m.alamat_pengirim   
															 FROM pesanan_master m 
															 where m.customer_seq = $customer_seq and m.tipe_customer = '$tipe_cust' $filter order by seq desc $limitBy ");
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});


$app->get('/web/pesanan/load', function (Request $request, Response $response, array $args) {
	$offset       = $request->getQueryParam("offset");
	$status       = $request->getQueryParam("status");
	$limit        = $request->getQueryParam("limit");
	$search       = $request->getQueryParam("search");
	
	if (empty($offset)){
		$offset = 0;
	}
	$filter = "";
	if (!empty($status) && ($status != "")){
		$filter .= " AND m.status = '$status'";
	}

  if (!empty($search)){
      $filter .= " AND ((m.nomor LIKE '%$search%') or (c.nama LIKE '%$search%') or (c.no_telp LIKE '%$search%') or 
                        (c.email LIKE '%$search%') or (c.alamat LIKE '%$search%'))";    
  }

	$limitBy = "";
	if (!empty($limit)){
		$limitBy .= "limit $limit OFFSET $offset";
	}
	
  $query = $this->db->prepare("SELECT m.seq as seq, m.tanggal, m.nomor, m.customer_seq as id_customer, m.total as total, m.diskon, m.subtotal, m.status, c.nama, c.kota, c.provinsi, 
															 c.alamat, c.no_telp, c.email, m.alamat_kirim, m.alamat_pengirim 
															 FROM pesanan_master m, master_customer c 
															 where m.customer_seq = c.seq $filter order by m.seq desc $limitBy ");
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});


$app->post('/web/pesanan/update_status', function (Request $request, Response $response, array $args) {

	$dataPost = $request->getParsedBody();
	$status = $dataPost['status'];
  $id = $dataPost['id'];
	$query = $this->db->prepare("UPDATE pesanan_master set status = '$status' where seq = $id");

	$result = $query->execute();
	if ($result) {
	 		return $response->withJson(["status" => "success", "id" => $id], 200);   
	}else{
	 		return $response->withJson(["status" => "failed", "id" => $id], 200);
	}
  return $response->withJson($data);
})->add($cekAPIKeyWeb);