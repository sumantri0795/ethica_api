<?php
use Slim\Http\Request;
use Slim\Http\Response;


$app->get('/web/customer/load', function (Request $request, Response $response, array $args) {       
    $order  = '';
    $filter = '';
    
    $order       = $request->getQueryParam("order_by");     
    $search      = $request->getQueryParam("search");   
    $offset      = $request->getQueryParam("offset");   
    $limit       = $request->getQueryParam("limit");    
    if (empty($offset)){
        $offset = 0;
    }

    $limitBy = "";
    if (!empty($limit)){
        $limitBy .= "limit $limit OFFSET $offset";
    }

    if (!empty($order)){
        $order = " ORDER BY b.$order";  
    }

    if (!empty($search)){
        $filter .= " AND ((nama LIKE '%$search%') or (user_id_ethica LIKE '%$search%') or (user_id_seply LIKE '%$search%') or 
                          (no_telp LIKE '%$search%') or (email LIKE '%$search%'))";    
    }
    $query = $this->db->prepare("SELECT seq as id, kode, nama, user_id_ethica, user_id_seply, no_telp, alamat, kota, provinsi, email, tgl_hapus FROM master_customer WHERE seq <> 0 $filter $order $limitBy ");                                                       
    $result = $query->execute();
    
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array(
                'kode' => 200,
                'keterangan' => 'Tidak ada data',
                'data' => null);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
  return $response->withJson($data);
});

$app->post('/web/customer/login', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();

    $user_id = $data['user'];
    $pass = $data['pass'];  
    
    $querySelect = $this->db->prepare("SELECT seq FROM master_customer WHERE user_id_ethica = '$user_id' AND password_ethica = '$pass' and is_ethica = 'T' and tgl_hapus is null AND user_id_ethica IS NOT NULL AND user_id_ethica <> '' ");
    $result   = $querySelect->execute(); 
    $rowCount = $querySelect->rowCount(); 
    $tipe     = "E";

    if ($rowCount == 0) {
        $querySelect = $this->db->prepare("SELECT seq FROM master_customer WHERE user_id_seply = '$user_id' AND password_seply = '$pass' and is_seply = 'T' and tgl_hapus is null AND user_id_seply IS NOT NULL AND user_id_ethica <> '' ");
        $result = $querySelect->execute();  
        $rowCount = $querySelect->rowCount(); 
        $tipe = "S";
    }

    if ($rowCount > 0) {
        $hasil = $querySelect->fetch();
        $seq = $hasil["seq"];
    }

    if ($seq > 0) {
        $query = $this->db->prepare("delete from api_users where user_id = '$user_id' and password = '$pass' and tipe_aplikasi = 'W'");
        $result = $query->execute();

        $api_key = RandomToken(32);
        $api_key = substr($api_key, 0, 31);

        $query = $this->db->prepare("insert into api_users (user_id, password, api_key, expired_date, tipe_aplikasi) values 
                                    (:user_id, :pass, :api_key, (SELECT Date(now())+ INTERVAL 1 MINUTE + INTERVAL 1 DAY), 'W')");
        $query->bindParam(':user_id', $data['user']);
        $query->bindParam(':pass', $data['pass']);
        $query->bindParam(':api_key', $api_key);
        $result = $query->execute();
        if($result){        
            return $response->withJson([
                "id" => $seq, 
                "api_key" => $api_key,
                "status"=>"success",
                "tipe"=>"$tipe"
                ], 200);       
        }else{
            return $response->withJson([
                "id" => "0", 
                "api_key" => "",
                "status"=>"Failed",
                "tipe"=>"$tipe"
                ], 200);       
        }
    }else{
        return $response->withJson([
            "id" => "0", 
            "api_key"=> "",
            "status"=>"Failed",
            "tipe"=>""
            ], 200);       
    }
});


$app->get('/web/customer/get/{id}', function (Request $request, Response $response, array $args) {
  $query = $this->db->prepare('SELECT seq as id, kode, nama, no_telp, alamat, kota, provinsi, email FROM master_customer where seq = :id');
  $query->bindParam(':id', $args['id']);
    $result = $query->execute();
    if ($result) {
        if ($query->rowCount()) {
            $data = $query->fetchAll();
        }else{
            $data = array(
                'kode' => 200,
                'keterangan' => 'Tidak ada data',
                'data' => null);
        }
    }else{
        $data = array(
            'kode' => 100,
            'keterangan' => 'Terdapat error',
            'data' => null);
    }
    return $response->withJson($data);
});


$app->post('/web/customer/change_password', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();

    $customer_seq = $data['id_customer'];
    $tipe_cust = $data['tipe'];
    $pass_lama = $data['pass_lama'];
    $pass_baru = $data['pass_baru'];

    $fieldPass = "password_ethica";
    $fieldUser = "user_id_ethica";
    if ($tipe_cust == "S"){
        $fieldUser = "user_id_seply";
        $fieldPass = "password_seply";
    }
    $querySelect = $this->db->prepare("SELECT $fieldUser as user_id FROM master_customer WHERE seq = '$customer_seq' AND $fieldPass = '$pass_lama' ");
    $querySelect->execute();
     
    $rowCount = $querySelect->rowCount(); 
    if ($rowCount ==  0) {
        return $response->withJson([
            "status"=>"Wrong Password"
            ], 200);   
    }
    $hasil = $querySelect->fetch();
    $user_id = $hasil["user_id"];

    $query = $this->db->prepare("update master_customer set $fieldPass = '$pass_baru' where seq = $customer_seq ");
    $query->execute();
     
    $query = $this->db->prepare("update api_users set password = '$pass_baru' where user_id = '$user_id' and tipe_aplikasi = 'W'");
    $result = $query->execute();

    if($result){        
        return $response->withJson([
            "status"=>"success",
            "tipe"=>"$tipe"
            ], 200);       
    }else{
        return $response->withJson([
            "status"=>"Failed",
            "tipe"=>"$tipe"
            ], 200);       
    }
})->add($cekAPIKeyWeb);


$app->post('/web/customer/update_profile', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();
    $customer_seq = $data['id_customer'];
    $nama = $data['nama'];
    $email = $data['email'];
    $telepon = $data['telepon'];
    $alamat = $data['alamat'];
    $kota = $data['kota'];
    $provinsi = $data['provinsi'];
    
    $query = $this->db->prepare("update master_customer set nama = '$nama', 
                                    email = '$email', 
                                    no_telp = '$telepon', 
                                    alamat = '$alamat', 
                                    kota = '$kota', 
                                    provinsi = '$provinsi' 
                                 where seq = $customer_seq ");
    $result = $query->execute();

    if($result){        
        return $response->withJson([
            "status"=>"success",
            "tipe"=>"$tipe"
            ], 200);       
    }else{
        return $response->withJson([
            "status"=>"Failed",
            "tipe"=>"$tipe"
            ], 100);       
    }
})->add($cekAPIKeyWeb);



