<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/web/product/load', function (Request $request, Response $response, array $args) {		
	$order  = '';
	$filter = '';	
	
	$brand         = $request->getQueryParam("brand"); 	
	$multi_brand   = $request->getQueryParam("multi_brand");
	$sub_brand     = $request->getQueryParam("sub_brand"); 	
	$warna         = $request->getQueryParam("warna"); 	
	$order         = $request->getQueryParam("order_by"); 	
	$search        = $request->getQueryParam("search"); 	
	$offset        = $request->getQueryParam("offset"); 	
	$limit         = $request->getQueryParam("limit"); 	
	$tipe_cust     = $request->getQueryParam("tipe_cust"); 	
	$harga_dari    = $request->getQueryParam("harga_dari"); 	
	$harga_sampai  = $request->getQueryParam("harga_sampai");
	$harga_sampai  = $request->getQueryParam("harga_sampai");
	$random        = $request->getQueryParam("random");

	if (!empty($harga_dari)){
		$filter .= " AND b.harga >= $harga_dari";
	}

	if ((!empty($harga_sampai)) && ($harga_sampai > 0)){
		$filter .= " AND b.harga <= $harga_sampai";
	}
	if (empty($offset)){
		$offset = 0;
	}

	$limitBy = "";
	if (!empty($limit)){
		$limitBy .= "limit $limit OFFSET $offset";
	}

	if (!empty($brand)){
		$filter .= " AND b.brand = '$brand'";
	}

	if (!empty($multi_brand)){
		$multi_brand = str_replace(",", "','", $multi_brand);
		$filter .= " AND b.brand IN ('$multi_brand') ";
	}	

	if (!empty($sub_brand)){
		$filter .= " AND b.sub_brand = '$sub_brand'";
	}

	if (!empty($warna)){
		$filter .= " AND b.warna = '$warna'";
	}

	if (!empty($order)){
		$order = " ORDER BY b.$order";	
	}

	if (!empty($random)){
		if ($random == "Y"){
			$order = " ORDER BY RAND()";	
		}			
	}	

	if (!empty($search)){		
		$filter .= " AND nama LIKE '%$search%'";			
	}

	if (!empty($tipe_cust)){
		if ($tipe_cust == "E"){ 
			$filter .= " AND b.brand <> 'SEPLY'";
		}
		if ($tipe_cust == "S"){ 
			$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI')";
		}
	}

	$query = $this->db->prepare("SELECT b.seq as id, b.barcode as kode, b.nama, b.brand, b.sub_brand, b.warna, 
								 b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar as gambar_tumbnail, b.gambar_sedang, b.gambar_besar, b.harga, b.tgl_hapus, s.stok 
								 FROM master_barang b, stok_barang s
								 WHERE b.seq = s.barang_seq AND b.tgl_hapus IS NULL and s.stok > 0 $filter $order 
								 $limitBy ");								 						
	$result = $query->execute();
	
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});

$app->get('/web/product/get/{id}', function (Request $request, Response $response, array $args) {
  $query = $this->db->prepare('SELECT b.seq as id, b.barcode as kode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar as gambar_tumbnail, b.gambar_sedang, b.gambar_besar, b.harga, b.tgl_hapus, s.stok FROM master_barang b, stok_barang s where s.barang_seq = b.seq and b.seq = :id');
  $query->bindParam(':id', $args['id']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

$app->get('/web/product/load_brand', function (Request $request, Response $response, array $args) {

	$filter = "";
	$tipe_cust   = $request->getQueryParam("tipe_cust"); 		

	if (!empty($tipe_cust)){
		if ($tipe_cust == "E"){ 
			$filter .= " AND brand <> 'SEPLY'";
		}
		if ($tipe_cust == "S"){ 
			$filter .= " AND brand in ('SEPLY', 'OHYA', 'MAJORI')";
		}
	}
	
	$query = $this->db->prepare("SELECT distinct brand FROM master_barang WHERE tgl_hapus IS NULL $filter ORDER BY brand");					
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});


$app->get('/web/product/load_sub_brand[/{brand}]', function (Request $request, Response $response, array $args) {
	$filter = '';
	if(!empty($args['brand'])){	
		$brand = $args['brand'];
		$filter = " AND brand = '$brand' ";	
	}
	$query = $this->db->prepare("SELECT distinct sub_brand FROM master_barang  WHERE tgl_hapus IS NULL $filter ORDER BY sub_brand");					
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

$app->get('/web/product/load_ukuran', function (Request $request, Response $response, array $args) {
	$query = $this->db->prepare("SELECT distinct ukuran FROM master_barang WHERE tgl_hapus IS NULL ORDER BY ukuran");				
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

$app->get('/web/product/load_warna', function (Request $request, Response $response, array $args) {
	$query = $this->db->prepare("SELECT distinct warna FROM master_barang WHERE tgl_hapus IS NULL ORDER BY warna");				
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});