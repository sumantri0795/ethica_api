<?php
use Slim\Http\Request;
use Slim\Http\Response;

$cekAPIKeyWeb = function($request, $response, $next){

    $key = $request->getQueryParam("key");    

    if(!isset($key)){
        return $response->withJson(["status" => "API Key required"]);
    }
    
    $sql = "SELECT * FROM api_users WHERE api_key=:api_key and expired_date > now() and tipe_aplikasi = 'W'";
    $stmt = $this->db->prepare($sql);
    $stmt->execute([":api_key" => $key]);
    
    if($stmt->rowCount() > 0){
        $result = $stmt->fetch();
        if($key == $result["api_key"]){
            $stmt->execute([":api_key" => $key]);   
            return $response = $next($request, $response);
        }
    }

    return $response->withJson(["status" => "Unauthorized"]);
};

$app->get('/web/api/cek', function (Request $request, Response $response) {
    return $response->withJson(["status" => "Authorized"]);        
})->add($cekAPIKeyWeb);