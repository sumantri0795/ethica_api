<?php
use Slim\Http\Request;
use Slim\Http\Response;

//Untuk mendapatkan daftar barang
$app->get('/master_barang/loaddata', function (Request $request, Response $response, array $args) {		
	$order  = '';
	$filter = '';		

	$brand      	 = $request->getQueryParam("brand"); 	
	$sub_brand  	 = $request->getQueryParam("sub_brand"); 		
	$warna			 = $request->getQueryParam("warna"); 	
	$tahun			 = $request->getQueryParam("tahun"); 	
	$order			 = $request->getQueryParam("order_by"); 		
	$search     	 = $request->getQueryParam("search"); 	
	$offset      	 = $request->getQueryParam("offset"); 	
	$tipe_cust		 = $request->getQueryParam("tipe_cust"); 
	$klasifikasi	 = $request->getQueryParam("klasifikasi"); //+new	
	$sarimbit_seq    = $request->getQueryParam("sarimbit_seq"); //+new
	$is_pre_order  	 = $request->getQueryParam("is_pre_order"); //+new	
	$is_ethica  	 = $request->getQueryParam("is_ethica"); //+new
	$is_seply  		 = $request->getQueryParam("is_seply"); //+new
	$is_ethica_hijab = $request->getQueryParam("is_ethica_hijab"); //+new
	$is_barang_promo = $request->getQueryParam("is_barang_promo"); //+new	

	$sub_brand = str_replace("'", "''", $sub_brand);
	$search = str_replace("'", "''", $search);
	$brand = str_replace("'", "''", $brand);
	$warna = str_replace("'", "''", $warna);

	//+new
	$KLA_DIAMOND     = "D";
	$KLA_SHAPPHIRE	 = "S";
	$KLA_EMERALD	 = "E";
	$KLA_RUBI		 = "R";
	$KLA_NOT_DIAMOND = "ND";
	//--

	if (!empty($brand)){
		$filter .= " AND b.brand = '$brand'";
	}

	if (!empty($sub_brand)){
		$filter .= " AND b.sub_brand = '$sub_brand'";
	}

	if (!empty($warna)){
		$filter .= " AND b.warna = '$warna'";
	}

	if (!empty($tahun)){
		$filter .= " AND b.tahun = '$tahun-01-01'";
	}

	if (!empty($order)){
		$order = " ORDER BY $order";	
	}

	if (!empty($search)){
		$filter .= " AND nama LIKE '%$search%'";	
	}

	// if (!empty($tipe_cust)){
	// 	if ($tipe_cust == "E"){
	// 		$filter .= " AND b.brand <> 'SEPLY'";
	// 	}
	// 	if ($tipe_cust == "S"){
	// 		$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI')";
	// 	}
	// }
	//+new
	$filterBrand   = "";	

	//+new
	if (!empty($is_ethica)){
		if ($is_ethica == "T"){
			$filterBrand .= " OR is_ethica ='T' ";					
		}		
	}	

	//+new
	if (!empty($is_seply)){
		if ($is_seply == "T"){
			$filterBrand .= " OR is_seply ='T' ";			
		}				
	}	

	//+new
	if (!empty($is_ethica_hijab)){
		if ($is_ethica_hijab == "T"){
			$filterBrand .= " OR is_ethicahijab ='T' ";			
		}						
	}	
	
	//+new
	if ($filterBrand != ""){
		$filterBrand = substr($filterBrand, 4);
		$filter .= " AND b.brand_seq in ( SELECT seq FROM master_brand WHERE ($filterBrand) )";
	}	

	//+new
	if (!empty($klasifikasi)){	
		switch ($klasifikasi) {
			case $KLA_DIAMOND :											   
				$filter .= " AND (DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(b.tgl_release) AND DATE(DATE_ADD(b.tgl_release, INTERVAL 3 MONTH)) OR (DATE(b.tgl_release) > DATE(DATE_FORMAT(NOW(),'%y-%m-%d')))) ";
				break;
			case $KLA_SHAPPHIRE :
				$filter .= " AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(b.tgl_release, INTERVAL 3 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(b.tgl_release, INTERVAL 12 MONTH)) ";
				break;
			case $KLA_EMERALD :
				$filter .= " AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(b.tgl_release, INTERVAL 12 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(b.tgl_release, INTERVAL 24 MONTH)) ";
				break;
			case $KLA_RUBI :
				$filter .= " AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) > DATE(DATE_ADD(DATE_ADD(b.tgl_release, INTERVAL 24 MONTH), INTERVAL 1 DAY))";
				break;
			case $KLA_NOT_DIAMOND :
				$filter .= " AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) > DATE(DATE_ADD(DATE_ADD(b.tgl_release, INTERVAL 3 MONTH), INTERVAL 1 DAY))";
			break;	
		}	
	}	
	
	//+new	
	if (!empty($sarimbit_seq)){
		if ($sarimbit_seq > 0){
			$filter .= " AND b.seq IN ( SELECT barang_seq FROM setting_sarimbit_detail sd ".
									"WHERE master_seq = $sarimbit_seq AND b.seq = sd.barang_seq ) ";
		}		
	}	    		

	//+new	
	if (!empty($is_pre_order)){
		$filter .= " AND b.is_preorder = '$is_pre_order' AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) >= DATE(b.berlaku_dari) AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) <= DATE(b.berlaku_sampai) ";
	}else{
		$filter .= " AND IFNULL(b.is_preorder,'') <> 'T' ";		
	}	

	$KondisiPromo = "";
	if (!empty($is_barang_promo)){
		if ($is_barang_promo == 'T'){			
			$KondisiPromo = " AND barang.diskon_pct > 0 ";
		}	
	}

	$FilterPromo = "LEFT JOIN setting_diskon_detail sd ON ((b.brand_seq = sd.brand_seq AND b.sub_brand_seq = sd.sub_brand_seq AND b.klasifikasi = sd.klasifik) ".
					"or (sd.brand_seq = 0 AND sd.sub_brand_seq = 0 AND sd.klasifik = 'Semua') ". // semua 
					"or (b.brand_seq = sd.brand_seq AND sd.sub_brand_seq = 0 AND sd.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
					"or (b.brand_seq = sd.brand_seq AND b.sub_brand_seq = sd.sub_brand_seq AND sd.klasifik = 'Semua') ". // klasifikasi semua
					"or (sd.brand_seq = 0 AND sd.sub_brand_seq = 0 AND b.klasifikasi = sd.klasifik) ". // brand dan sub brand semua 
					"or (sd.brand_seq = 0 AND sd.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = sd.klasifik) ". // brand sama
					"or (b.brand_seq = sd.brand_seq AND sd.sub_brand_seq = 0 AND b.klasifikasi = sd.klasifik) ". // sub brand semua 
                    ") ".
					"AND sd.master_seq IN (SELECT sm.seq FROM setting_diskon_master sm WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) AND sm.tgl_hapus IS NULL ) ";
				

	
	if ($is_pre_order <> 'T'){
		$sql = 	"SELECT seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, SUM(stok) AS stok, ".
            	"gambar_besar, keterangan, is_ehijab, is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, tgl_release, IFNULL(diskon_pct,0) AS diskon_pct, klasifikasi FROM (".
					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, p.qty AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, ".
					"MAX(sd.diskon_pct) AS diskon_pct, p.seq AS SeqTrans, b.klasifikasi ".
					"FROM penambah_pengurang_stok p, master_barang b $FilterPromo ".
					"WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter ".   
					"GROUP BY b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, stok, ".
					"gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, SeqTrans".
					
					" UNION ALL ".

					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, -p.qty AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, ".
					"MAX(sd.diskon_pct) AS diskon_pct, p.seq AS SeqTrans, b.klasifikasi ".
					"FROM pesanan_detail p, master_barang b $FilterPromo ".
					"WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter ".					
					"AND p.master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H','S') AND is_data_lama <> 'T') ".
					"GROUP BY b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, stok, ".
					"gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, SeqTrans ".

					" UNION ALL ".

					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, -p.qty AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, ".
					"MAX(sd.diskon_pct) AS diskon_pct, p.seq AS SeqTrans, b.klasifikasi ".
					"FROM keranjang p, master_barang b $FilterPromo ".					
					"WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter ".   
					"GROUP BY b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, stok, ".
					"gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, b.berlaku_dari, b.berlaku_sampai, b.jumlah_pesan, b.tgl_release, SeqTrans ".

				") AS barang ".
				"GROUP BY seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, gambar_besar, keterangan, is_ehijab, ".
				"is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, tgl_release, diskon_pct, klasifikasi ".
				"HAVING SUM(barang.stok) $KondisiPromo ".
				"$order limit 30 OFFSET $offset";
				
	}else if ($is_pre_order == 'T'){

		$sql = 	"SELECT seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, SUM(stok) AS stok, ".
				"gambar_besar, keterangan, is_ehijab, is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, tgl_release, diskon_pct, klasifikasi FROM (".					
					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, b.jumlah_pesan AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, DATE(DATE_FORMAT(b.berlaku_dari,'%y-%m-%d')) as berlaku_dari, DATE(DATE_FORMAT(b.berlaku_sampai,'%y-%m-%d')) as berlaku_sampai,".
					"b.jumlah_pesan, b.tgl_release, 0 AS diskon_pct, b.klasifikasi ".
					"FROM master_barang b ".
					"WHERE b.tgl_hapus IS NULL $filter ".

					" UNION ALL ".

					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, -p.qty AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, DATE(DATE_FORMAT(b.berlaku_dari,'%y-%m-%d')) as berlaku_dari, DATE(DATE_FORMAT(b.berlaku_sampai,'%y-%m-%d')) as berlaku_sampai, ".
					"b.jumlah_pesan, b.tgl_release, 0 AS diskon_pct, b.klasifikasi ".
					"FROM master_barang b, pesanan_detail p ".
					"WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter ".
					"AND p.master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H') AND jenis_so ='P' AND is_data_lama <> 'T')".

					" UNION ALL ".

					"SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, -p.qty AS stok, ".
					"b.gambar_sedang as gambar_besar, b.keterangan, b.is_ehijab, b.is_preorder, DATE(DATE_FORMAT(b.berlaku_dari,'%y-%m-%d')) as berlaku_dari, DATE(DATE_FORMAT(b.berlaku_sampai,'%y-%m-%d')) as berlaku_sampai, ".
					"b.jumlah_pesan, b.tgl_release, 0 AS diskon_pct, b.klasifikasi ".
					"FROM master_barang b, keranjang p ".
					"WHERE b.seq = p.barang_seq AND b.tgl_hapus IS NULL $filter ".           

				") AS barang ".
				"GROUP BY seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, gambar_besar, keterangan, is_ehijab, ".
				"is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, tgl_release, diskon_pct, klasifikasi ".
				//"HAVING SUM(barang.stok) > 0 ".
				"$order limit 30 OFFSET $offset";				
	}
	
	$query = $this->db->prepare($sql);
	$result = $query->execute();	
	
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}	
  return $response->withJson($data);
});


$app->get('/master_barang/get/{seq}', function (Request $request, Response $response, array $args) {
	$sql = "SELECT seq, barcode, nama, brand, sub_brand, warna, ukuran, katalog, tahun, tipe_brg, gambar, harga, tgl_hapus, ".
			"gambar_besar, gambar_sedang, keterangan, tgl_release, is_ehijab, is_preorder, berlaku_dari, berlaku_sampai, jumlah_pesan, ".
			"brand_seq, sub_brand_seq, berat, klasifikasi ".
			"FROM master_barang WHERE seq = :seq ";
	$query = $this->db->prepare($sql);
	$query->bindParam(':seq', $args['seq']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});


//Untuk mendapatkan daftar brand
$app->get('/master_barang/load_brand', function (Request $request, Response $response, array $args) {

	$filter = "";
	$tipe_cust   = $request->getQueryParam("tipe_cust"); 	

	if (!empty($tipe_cust)){
		if ($tipe_cust == "E"){ 
			$filter .= " AND brand <> 'SEPLY'";
		}
		if ($tipe_cust == "S"){ 
			$filter .= " AND brand in ('SEPLY', 'OHYA', 'MAJORI')";
		}
	}
	
	$query = $this->db->prepare("SELECT distinct brand FROM master_barang WHERE tgl_hapus IS NULL $filter ORDER BY brand");					
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});


//Untuk mendapatkan daftar sub brand
$app->get('/master_barang/load_sub_brand[/{brand}]', function (Request $request, Response $response, array $args) {
	$filter = '';
	if(!empty($args['brand'])){	
		$brand = $args['brand'];
		$filter = " AND brand = '$brand' ";	
	}
	$query = $this->db->prepare("SELECT distinct sub_brand FROM master_barang  WHERE tgl_hapus IS NULL $filter ORDER BY sub_brand");					
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

$app->get('/master_barang/load_ukuran', function (Request $request, Response $response, array $args) {
	$query = $this->db->prepare("SELECT distinct ukuran FROM master_barang WHERE tgl_hapus IS NULL ORDER BY ukuran");				
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

//Untuk mendapatkan daftar warna
$app->get('/master_barang/load_warna', function (Request $request, Response $response, array $args) {
	$query = $this->db->prepare("SELECT distinct warna FROM master_barang WHERE tgl_hapus IS NULL ORDER BY warna");				
	$result = $query->execute();
	
	if ($result) {

		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
    return $response->withJson($data);
});

//==============================================================  API LAMA ===========================================================\\
$app->get('/master_barang/loaddata_old', function (Request $request, Response $response, array $args) {		
	$order  = '';
	$filter = '';
	
	$brand       = $request->getQueryParam("brand"); 	
	$sub_brand   = $request->getQueryParam("sub_brand"); 	
	
	$warna       = $request->getQueryParam("warna"); 	
	$tahun       = $request->getQueryParam("tahun"); 	
	$order       = $request->getQueryParam("order_by"); 	
	$search      = $request->getQueryParam("search"); 	
	$offset      = $request->getQueryParam("offset"); 	
	$tipe_cust   = $request->getQueryParam("tipe_cust"); 

	$sub_brand = str_replace("'", "''", $sub_brand);
	$search = str_replace("'", "''", $search);
	$brand = str_replace("'", "''", $brand);
	$warna = str_replace("'", "''", $warna);

	if (!empty($brand)){
		$filter .= " AND b.brand = '$brand'";
	}

	if (!empty($sub_brand)){
		$filter .= " AND b.sub_brand = '$sub_brand'";
	}

	if (!empty($warna)){
		$filter .= " AND b.warna = '$warna'";
	}

	if (!empty($tahun)){
		$filter .= " AND b.tahun = '$tahun-01-01'";
	}

	if (!empty($order)){
		$order = " ORDER BY b.$order";	
	}

	if (!empty($search)){
		$filter .= " AND nama LIKE '%$search%'";	
	}

	if (!empty($tipe_cust)){
		if ($tipe_cust == "E"){ 
			$filter .= " AND b.brand <> 'SEPLY'";
		}
		if ($tipe_cust == "S"){ 
			$filter .= " AND b.brand in ('SEPLY', 'OHYA', 'MAJORI')";
		}
	}

	$query = $this->db->prepare("SELECT b.seq, b.barcode, b.nama, b.brand, b.sub_brand, b.warna, 
								 b.ukuran, b.katalog, b.tahun, b.tipe_brg, b.gambar, b.harga, b.tgl_hapus, s.stok, b.gambar_sedang as gambar_besar, b.keterangan 
								 FROM master_barang b, stok_barang s
								 WHERE b.seq = s.barang_seq AND b.tgl_hapus IS NULL and s.stok > 0  $filter $order 
								 limit 30 OFFSET $offset");								 						
	$result = $query->execute();
	
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});

$app->get('/master_barang/get_diskon/{id}', function (Request $request, Response $response, array $args) {
	$sql = 	"SELECT range_mulai, range_sampai, diskon_pct ".
			"FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
			"WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
			"AND ((s.brand_seq = b.brand_Seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = b.klasifikasi) ".					
					"or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
					"or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
					"or (b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
					"or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
					"or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
					"or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 

			") AND sm.tgl_hapus IS NULL AND b.seq = :id ORDER BY diskon_pct ";
	$query = $this->db->prepare($sql);
	$query->bindParam(':id', $args['id']);
	$data = array();
	$result = $query->execute();
	  if ($result) {
		    if ($query->rowCount()) {
				$data = $query->fetchAll();		    
			}
	  }else{
		  $data = array(
			  'kode' => 100,
			  'keterangan' => 'Terdapat error',
			  'data' => null);
	  }
	  return $response->withJson($data);
});


$app->post('/master_barang/set_klasifikasi', function (Request $request, Response $response) {
	$db = $this->db;
	try {
		$db->beginTransaction();
		$sql = 	"UPDATE master_barang SET klasifikasi = 'Diamond' ". 
				"WHERE (DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(tgl_release) AND DATE(DATE_ADD(tgl_release, INTERVAL 3 MONTH)) OR (DATE(tgl_release) > DATE(DATE_FORMAT(NOW(),'%y-%m-%d'))))";
		$query = $db->prepare($sql);
		$query->execute();

		$sql = 	"UPDATE master_barang SET klasifikasi = 'Shapphire' ".
				"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 3 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(tgl_release, INTERVAL 12 MONTH))";
		$query = $db->prepare($sql);
		$query->execute();

		$sql = 	"UPDATE master_barang SET klasifikasi = 'Emerald' ".
				"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 12 MONTH), INTERVAL 1 DAY)) AND DATE(DATE_ADD(tgl_release, INTERVAL 24 MONTH))";
		$query = $db->prepare($sql);
		$query->execute();

		$sql = 	"UPDATE master_barang SET klasifikasi = 'Rubi' ".
				"WHERE DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) > DATE(DATE_ADD(DATE_ADD(tgl_release, INTERVAL 24 MONTH), INTERVAL 1 DAY))";
		$query = $db->prepare($sql);
		$query->execute();	
		$db->commit();

	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
    }
	return $response->withJson(["status" => "success"], 200);
});

//////////////////////////

// $app->get('/master_barang/test', function (Request $request, Response $response, array $args) {
// 	$data = cek($this->db);	
// 	die(var_dump($data));

//     return $response->withJson($data);
// });


// function cek ($db){
// 	$query = $db->prepare("SELECT distinct ukuran FROM master_barang WHERE tgl_hapus IS NULL ORDER BY ukuran");				
// 	$result = $query->execute();
	
// 	if ($result) {

// 		if ($query->rowCount()) {
// 			$data = $query->fetchAll();
// 		}else{
// 			$data = array(
// 				'kode' => 200,
// 				'keterangan' => 'Tidak ada data',
// 				'data' => null);
// 		}
// 	}else{
// 		$data = array(
// 			'kode' => 100,
// 			'keterangan' => 'Terdapat error',
// 			'data' => null);
// 	}
//     return $data;
// }