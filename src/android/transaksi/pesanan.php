<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../../src/global/engine_global.php';

$app->post('/pesanan_master/save', function (Request $request, Response $response) {
	$db   = $this->db;
	try {
		$db->beginTransaction();
		$dataPost = $request->getParsedBody();

		$diskonEthica = 0;
		$diskonSeply  = 0;

		$tipe_cust    = $dataPost['tipe_cust'];  	
		$customer_seq = $dataPost['customer_seq'];
		$user_id	  = $dataPost['user_id'];
		$tipe_apps    = $dataPost['tipe_apps'];
		
		if (empty($tipe_cust)){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);   		
		}

		if ($tipe_cust == ""){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);  				
		}

		if (empty($customer_seq)){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);
		}

		if ($customer_seq == 0){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);
		}
		$query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");			
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}					
				}						
			}
		}

		//untuk mencegah kalo detailnya gaada tapi masternya ada
		$sql = 	"SELECT COUNT(*) AS jumlah FROM keranjang k, master_customer c ".
				"WHERE k.qty > 0 and k.customer_seq = c.seq and k.customer_seq = :customer_seq AND k.tipe_customer = '$tipe_cust'";			
		$query = $db->prepare($sql);			
		$query->bindParam(':customer_seq', $dataPost['customer_seq']);
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {	
				$data = $query->fetch();			
				$jumlahDetail =  $data["jumlah"];
				if ($jumlahDetail <= 0){
					return $response->withJson(["status" => "tidak ada detail", "seq" => 0], 200);					
				}
			}
		}
		
		$sql = "SELECT pct_diskon_ethica, pct_diskon_Seply, pct_diskon_ethica_hijab FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id' ";
		$query = $db->prepare($sql);
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["pct_diskon_ethica"])){
					$diskonEthica =  $data["pct_diskon_ethica"];
				}else{
					$diskonEthica = 0;
				}			

				if (!empty($data["pct_diskon_Seply"])){
					$diskonSeply  =  $data["pct_diskon_Seply"];
				}else{
					$diskonSeply  = 0;
				}		

				if (!empty($data["pct_diskon_ethica_hijab"])){
					$diskonEthicaHijab  =  $data["pct_diskon_ethica_hijab"];
				}else{
					$diskonEthicaHijab  = 0;
				}		
			}
		}	

		$sql = "INSERT INTO pesanan_master (tanggal, nomor, customer_seq, total, status, tipe_customer, is_kirim, subtotal, diskon, diskon_ethica, diskon_seply, diskon_ethica_hijab, ".
											"alamat_kirim, alamat_pengirim, tipe_aplikasi, user_id, total_berat, jenis_so, is_dropship, no_resi, nama_pengirim, ".
											"alamat_pengirim_lengkap, no_telepon_pengirim, nama_kirim, id_provinsi , id_kota , id_kecamatan, kelurahan, provinsi, ".
											"kota, kecamatan, alamat_kirim_lengkap, no_telepon_kirim, ongkos_kirim, ekspedisi, ekspedisi_seq , service, is_selesai_chat, is_data_lama, tgl_input)   ".
				"VALUES( now(), (select CONCAT('SO-A/', DATE_FORMAT(now(), '%m%y'), '/', lpad((IFNULL(max(substr(nomor,length(nomor)-3, 4)),0)+1),4,'0')) FROM pesanan_master sm ".
				"WHERE DATE_FORMAT(tanggal, '%m%y') = DATE_FORMAT(now(), '%m%y')), ".
				":customer_seq, :total, 'B', :tipe_cust, 'F', :subtotal, :diskon, $diskonEthica, $diskonSeply, $diskonEthicaHijab, :alamat_kirim, :alamat_pengirim, '$tipe_apps', ".
				":user_id, :total_berat, :jenis_so, :is_dropship, :no_resi, :nama_pengirim, :alamat_pengirim_lengkap, :no_telepon_pengirim, :nama_kirim, :id_provinsi , :id_kota , ".
				":id_kecamatan, :kelurahan, :provinsi, :kota, :kecamatan, :alamat_kirim_lengkap, :no_telepon_kirim, :ongkos_kirim, :ekspedisi, :ekspedisi_seq, :service, 'F', 'F', NOW())";

		$query = $db->prepare($sql);
		$query->bindParam(':total', $dataPost['total']);
		$query->bindParam(':customer_seq', $dataPost['customer_seq']);
		$query->bindParam(':tipe_cust', $dataPost['tipe_cust']);
		$query->bindParam(':diskon', $dataPost['diskon']);
		$query->bindParam(':subtotal', $dataPost['subtotal']);	
		$query->bindParam(':alamat_kirim', $dataPost['alamat_kirim']);	
		$query->bindParam(':alamat_pengirim', $dataPost['alamat_pengirim']);		
		$query->bindParam(':user_id', $dataPost['user_id']);
		$query->bindParam(':total_berat', $dataPost['total_berat']);
		$query->bindParam(':jenis_so', $dataPost['jenis_so']);
		$query->bindParam(':is_dropship', $dataPost['is_dropship']);
		$query->bindParam(':no_resi', $dataPost['no_resi']);
		$query->bindParam(':nama_pengirim', $dataPost['nama_pengirim']);
		$query->bindParam(':alamat_pengirim_lengkap',$dataPost['alamat_pengirim_lengkap']);
		$query->bindParam(':no_telepon_pengirim', $dataPost['no_telepon_pengirim']);
		$query->bindParam(':nama_kirim', $dataPost['nama_kirim']);
		$query->bindParam(':id_provinsi', $dataPost['id_provinsi']);
		$query->bindParam(':id_kota', $dataPost['id_kota']);
		$query->bindParam(':id_kecamatan', $dataPost['id_kecamatan']);
		$query->bindParam(':kelurahan', $dataPost['kelurahan']);
		$query->bindParam(':provinsi', $dataPost['provinsi']);
		$query->bindParam(':kota', $dataPost['kota']);
		$query->bindParam(':kecamatan', $dataPost['kecamatan']);
		$query->bindParam(':alamat_kirim_lengkap',$dataPost['alamat_kirim_lengkap']);
		$query->bindParam(':no_telepon_kirim', $dataPost['no_telepon_kirim']);
		$query->bindParam(':ongkos_kirim', $dataPost['ongkos_kirim']);
		$query->bindParam(':ekspedisi', $dataPost['ekspedisi']);
		$query->bindParam(':ekspedisi_seq', $dataPost['ekspedisi_seq']);
		$query->bindParam(':service', $dataPost['service']);	
		$result = $query->execute();
		//$id = $this->db->lastInsertId();

		$id = 0;
		$querySelect = $db->prepare('SELECT MAX(seq) AS seq FROM pesanan_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer AND user_id = :user_id');
		$querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);  
		$querySelect->bindParam(':tipe_customer', $dataPost['tipe_cust']);  
		$querySelect->bindParam(':user_id', $dataPost['user_id']);  
		$result = $querySelect->execute();
		if ($result) {
			if ($querySelect->rowCount()) {
						$data = $querySelect->fetch();        				
						$id = $data["seq"];        
			}
		}		

		$tipe_cust   = $dataPost['tipe_cust'];			
		$fielddiskon = "";
		if (!empty($tipe_cust)){
			if ($tipe_cust == "E"){ 				
				$fielddiskon = "((c.diskon_ethica * b.harga)/100)";
			}
			if ($tipe_cust == "S"){ 		
				$fielddiskon = "((c.diskon_seply * b.harga)/100)";
			}
		}	

		$sql =	"INSERT INTO pesanan_detail (master_seq, barang_seq, qty, harga, total, is_kirim, diskon, subtotal, tipe_aplikasi, berat) ".
				"SELECT $id, k.barang_seq, k.qty_order, b.harga, (b.harga-$fielddiskon) * k.qty, 'F', $fielddiskon, (b.harga) * k.qty, k.tipe_aplikasi, k.berat ".
				"FROM keranjang k, master_barang b, master_customer c ".
				"WHERE b.seq = k.barang_seq and k.qty_order > 0 and k.customer_seq = c.seq and k.customer_seq = :customer_seq ".
				"and k.tipe_customer = :tipe_customer AND k.user_id = :user_id AND is_ceklis = 'T' ";

		$query2 = $db->prepare($sql);
		$query2->bindParam(':customer_seq', $dataPost['customer_seq']);		
		$query2->bindParam(':user_id', $dataPost['user_id']);
		$query2->bindParam(':tipe_customer', $dataPost['tipe_cust']);
		$result = $query2->execute(); 

		//update ke qty terpesan
		$sql = 	"UPDATE keranjang SET qty_terpesan = qty_terpesan + :qty_order, qty = qty - :qty_order ".
				"WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer AND user_id = :user_id AND is_ceklis = 'T' AND qty_order > 0 ";
		$query5 = $db->prepare($sql);
		$query5->bindParam(':customer_seq', $dataPost['customer_seq']);		
		$query5->bindParam(':tipe_customer', $dataPost['tipe_customer']);
		$query5->bindParam(':user_id', $dataPost['user_id']);                
		$result = $query5->execute();

		//Save ke dml
		$sql = 	"SELECT seq, qty_order FROM keranjang ".
				"WHERE customer_seq = $customer_seq AND tipe_customer = '$tipe_cust' AND user_id = '$user_id' AND is_ceklis = 'T' AND qty_order > 0 ";
		$stmt2 = $db->prepare($sql);
		$stmt2->execute();
		$array = $stmt2->fetchAll();

		foreach($array as $row) {      
			$seq_keranjang = $row['seq'];            
			$qty_order     = $row['qty_order']; 

			$sql_1 = "UPDATE keranjang SET qty_terpesan = qty_terpesan + $qty_order, qty = qty - $qty_order WHERE seq = $seq_keranjang ";
			$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
						"VALUES ('$sql_1', 'F')";
			$query_dml  = $db->prepare($sql);                
			$query_dml->execute();                          
		} 	

		//Delete dari keranjang jika sudah diorder
		$sql =  "DELETE FROM keranjang WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer AND user_id = :user_id AND is_ceklis = 'T' AND qty_order > 0 AND qty = 0 ";	
		$query3 = $db->prepare($sql);
		$query3->bindParam(':customer_seq', $dataPost['customer_seq']);
		$query3->bindParam(':tipe_customer', $dataPost['tipe_cust']);
		$query3->bindParam(':user_id', $dataPost['user_id']);
		$result = $query3->execute();	

		//Save ke dml
		$sql_1 = "DELETE FROM keranjang WHERE customer_seq = $customer_seq AND tipe_customer = '$tipe_cust' AND user_id = '$user_id' AND qty = 0 ";	
		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
				 "VALUES ('$sql_1', 'F')";
		$query_dml2  = $db->prepare($sql);                
		$$query_dml2->execute();          
		
		
		$sql = "SELECT seq FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq AND tipe_customer = :tipe_customer AND user_id = :user_id";
		$querySelect = $db->prepare($sql);
		$querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);
		$querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);
		$querySelect->bindParam(':tipe_customer', $dataPost['tipe_cust']);
		$querySelect->bindParam(':user_id', $dataPost['user_id']);
		$result = $querySelect->execute(); 
		$rowCount = $querySelect->rowCount();

		if ($rowCount > 0) {
			$sql = "DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND tipe_customer = :tipe_customer AND user_id = :user_id";
			$query4 = $db->prepare($sql);
			$query4->bindParam(':customer_seq', $dataPost['customer_seq']);
			$query4->bindParam(':tipe_customer', $dataPost['tipe_cust']);	
			$query4->bindParam(':user_id', $dataPost['user_id']);	
			$result = $query4->execute();
		}
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
	}		
  	return $response->withJson(["status" => "success", "seq" => $id], 200);   
})->add($cekAPIKey);



//Untuk menyimpan keep pesanan dari cart
$app->post('/pesanan_master/keep_pesanan', function (Request $request, Response $response) {
	$db   = $this->db;
	try {
		$db->beginTransaction();
		$dataPost = $request->getParsedBody();

		$diskonEthica = 0;
		$diskonSeply  = 0;


		$customer_seq = $dataPost['customer_seq'];
		$user_id	  = $dataPost['user_id'];
		$nama_pengirim	  = $dataPost['nama_pengirim'];
		$alamat_pengirim_lengkap = $dataPost['alamat_pengirim_lengkap'];
		$no_telepon_pengirim	 = $dataPost['no_telepon_pengirim'];
		$nama_kirim	  		 	 = $dataPost['nama_kirim'];
		$kelurahan	  		  	 = $dataPost['kelurahan'];
		$provinsi	  		  	 = $dataPost['provinsi'];
		$kota	  			  	 = $dataPost['kota'];
		$kecamatan	  		  	 = $dataPost['kecamatan'];
		$alamat_kirim_lengkap 	 = $dataPost['alamat_kirim_lengkap'];
		$no_telepon_kirim	     = $dataPost['no_telepon_kirim'];
		$ongkos_kirim	  	  	 = $dataPost['ongkos_kirim'];
		$ekspedisi_seq	  	  	 = $dataPost['ekspedisi_seq'];
		$is_selesai_chat	  	 = $dataPost['is_selesai_chat'];
		$id_provinsi	  	  	 = $dataPost['id_provinsi'];
		$id_kota	  		  	 = $dataPost['id_kota'];
		$id_kecamatan	  	  	 = $dataPost['id_kecamatan'];
		$tipe_apps	  	  	 	 = $dataPost['tipe_apps'];
		$no_resi	  	  	 	 = $dataPost['no_resi'];


        $alamat_pengirim_lengkapTemp = "Gudang Ethica Group"."\n".
        "Jalan Mochammad Toha No. 398,"."\n".
		"Wates, Kecamatan Bandung Kidul,"."\n".
		"Kota Bandung, Jawa Barat 40243"."\n".
		"\n".
		"No. HP : ";
		$alamat_pengirim = $dataPost['alamat_pengirim'];	
		$is_dropship = "F";
		if ($alamat_pengirim_lengkapTemp != $alamat_pengirim){
			$is_dropship = "T";		
		}
		
		if (empty($customer_seq)){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);
		}
		if ($customer_seq == 0){
			return $response->withJson(["status" => "error customer", "seq" => 0], 200);
		}

		$query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}
				}
			}
		}

		//untuk mencegah kalo detailnya gaada tapi masternya ada
		$sql = 	"SELECT COUNT(*) AS jumlah FROM keranjang k, master_customer c ".
				"WHERE k.qty > 0 and k.customer_seq = c.seq and k.customer_seq = :customer_seq AND k.user_id = '$user_id'";			
		$query = $db->prepare($sql);			
		$query->bindParam(':customer_seq', $dataPost['customer_seq']);
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {	
				$data = $query->fetch();			
				$jumlahDetail =  $data["jumlah"];
				if ($jumlahDetail <= 0){
					return $response->withJson(["status" => "tidak ada detail", "seq" => 0], 200);					
				}
			}
		}

		//Cek plafon
		if (!getPlafonTerpakai($db, $dataPost['customer_seq'], 0, $dataPost['user_id'], 0, $dataPost['ongkos_kirim'], "qty_order")){
			return $response->withJson(["status" => "Plafon tidak mencukupi", "data" => "1", "plafon" => 0], 200);
		}
		

		$query = $db->prepare("SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab FROM detail_customer WHERE user_id = :user_id");		
		$query->bindParam(':user_id', $dataPost['user_id']);
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["pct_diskon_ethica"])){
					$diskonEthica =  $data["pct_diskon_ethica"];
				}else{
					$diskonEthica = 0;
				}
				if (!empty($data["pct_diskon_seply"])){
					$diskonSeply  =  $data["pct_diskon_seply"];
				}else{
					$diskonSeply  = 0;
				}		
				if (!empty($data["pct_diskon_ethica_hijab"])){
					$diskonEthicaHijab  =  $data["pct_diskon_ethica_hijab"];
				}else{
					$diskonEthicaHijab  = 0;
				}		
			}
		}

		$PrefixSO = 'SO';
		if ($dataPost['jenis_so'] == 'P'){
			$PrefixSO = 'PO';			
		}

		$sql = "INSERT INTO pesanan_master (tanggal, nomor, customer_seq, total, status, tipe_customer, is_kirim, subtotal, diskon, diskon_ethica, diskon_seply, ".
											"alamat_kirim, alamat_pengirim, tipe_aplikasi, user_id, total_berat, jenis_so, is_dropship, is_selesai_chat, nama_pengirim, alamat_pengirim_lengkap, no_telepon_pengirim, nama_kirim, kelurahan, provinsi, kota, kecamatan, alamat_kirim_lengkap, no_telepon_kirim, ongkos_kirim, ekspedisi, ekspedisi_seq, service,  id_provinsi, id_kota, id_kecamatan, diskon_ethica_hijab, no_resi, is_data_lama, tgl_input)   ".

				"VALUES( now(), (select CONCAT('$PrefixSO-A/', DATE_FORMAT(now(), '%m%y'), '/', lpad((IFNULL(max(substr(nomor,length(nomor)-3, 4)),0)+1),4,'0')) FROM pesanan_master sm ".
				"WHERE DATE_FORMAT(tanggal, '%m%y') = DATE_FORMAT(now(), '%m%y')), ".
				":customer_seq, :total, 'K', '', 'F', :subtotal, :diskon, $diskonEthica, $diskonSeply, :alamat_kirim, :alamat_pengirim, '$tipe_apps', ".
				":user_id, :total_berat, :jenis_so, :is_dropship, 'F', :nama_pengirim, :alamat_pengirim_lengkap, :no_telepon_pengirim, :nama_kirim, :kelurahan, :provinsi, :kota, :kecamatan, :alamat_kirim_lengkap, :no_telepon_kirim, :ongkos_kirim, :ekspedisi, :ekspedisi_seq, :service, :id_provinsi, :id_kota, :id_kecamatan,
				:diskonEthicaHijab, :no_resi, 'F', NOW()".
				")";
		$query = $db->prepare($sql);
		$query->bindParam(':customer_seq', $dataPost['customer_seq']);
		$query->bindParam(':total', $dataPost['total']);
		$query->bindParam(':subtotal', $dataPost['subtotal']);	
		$query->bindParam(':diskon', $dataPost['diskon']);
		$query->bindParam(':alamat_kirim', $dataPost['alamat_kirim']);	
		$query->bindParam(':alamat_pengirim', $dataPost['alamat_pengirim']);		
		$query->bindParam(':user_id', $dataPost['user_id']);
		$query->bindParam(':total_berat', $dataPost['total_berat']);
		$query->bindParam(':jenis_so', $dataPost['jenis_so']);
		$query->bindParam(':is_dropship', $dataPost['is_dropship']);
		$query->bindParam(':nama_pengirim', $dataPost['nama_pengirim']);	
		$query->bindParam(':alamat_pengirim_lengkap', $dataPost['alamat_pengirim_lengkap']);	
		$query->bindParam(':no_telepon_pengirim', $dataPost['no_telepon_pengirim']);	
		$query->bindParam(':nama_kirim', $dataPost['nama_kirim']);	
		$query->bindParam(':kelurahan', $dataPost['kelurahan']);	
		$query->bindParam(':provinsi', $dataPost['provinsi']);	
		$query->bindParam(':kota', $dataPost['kota']);	
		$query->bindParam(':kecamatan', $dataPost['kecamatan']);	
		$query->bindParam(':alamat_kirim_lengkap', $dataPost['alamat_kirim_lengkap']);	
		$query->bindParam(':no_telepon_kirim', $dataPost['no_telepon_kirim']);	
		$query->bindParam(':ongkos_kirim', $dataPost['ongkos_kirim']);	
		$query->bindParam(':ekspedisi_seq', $dataPost['ekspedisi_seq']);
		$query->bindParam(':ekspedisi', $dataPost['ekspedisi']);	
		$query->bindParam(':service', $dataPost['service']);
		$query->bindParam(':is_dropship', $is_dropship);	
		$query->bindParam(':id_provinsi', $dataPost['id_provinsi']);	
		$query->bindParam(':id_kota', $dataPost['id_kota']);	
		$query->bindParam(':id_kecamatan', $dataPost['id_kecamatan']);
		$query->bindParam(':diskonEthicaHijab', $diskonEthicaHijab);
		$query->bindParam(':no_resi', $no_resi);

		$result = $query->execute();
		//$id = $this->db->lastInsertId();

		$id = 0;
		$querySelect = $db->prepare('SELECT MAX(seq) AS seq FROM pesanan_master WHERE customer_seq = :customer_seq AND user_id = :user_id');
		$querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);  
		$querySelect->bindParam(':user_id', $dataPost['user_id']);  
		$result = $querySelect->execute();
		if ($result) {
			if ($querySelect->rowCount()) {
						$data = $querySelect->fetch();        				
						$id = $data["seq"];        
			}
		}		

		$sql =	"INSERT INTO pesanan_detail (master_seq, barang_seq, qty, harga, total, is_kirim, diskon, subtotal, tipe_aplikasi, berat) ".
				"SELECT $id, k.barang_seq, k.qty_order, b.harga, (b.harga -  ((b.harga / 100) * k.diskon)  ) * k.qty_order, 'F', ((b.harga / 100) * k.diskon), (b.harga) * k.qty_order, k.tipe_aplikasi, b.berat ".
				"FROM keranjang k, master_barang b, master_customer c ".
				"WHERE b.seq = k.barang_seq and k.qty_order > 0 and k.customer_seq = c.seq and k.customer_seq = :customer_seq ".
				"and  k.user_id = :user_id AND is_ceklis = 'T' ";
		$query2 = $db->prepare($sql);
		$query2->bindParam(':customer_seq', $dataPost['customer_seq']);		
		$query2->bindParam(':user_id', $dataPost['user_id']);
		$result = $query2->execute(); 

		//Save ke dml
		$sql = 	"SELECT seq, qty_order FROM keranjang ".
				"WHERE customer_seq = $customer_seq AND user_id = '$user_id' AND qty_order > 0 ";
		$stmt2 = $db->prepare($sql);
		$stmt2->execute();
		$array = $stmt2->fetchAll();

		foreach($array as $row) {      
			$seq_keranjang = $row['seq'];            
			$qty_order     = $row['qty_order']; 

			$sql_1 = "UPDATE keranjang SET qty_order = 0,  qty = qty - $qty_order WHERE seq = $seq_keranjang ";

			$query  = $db->prepare($sql_1); 
			$query->execute();                                  


			$sql_1 = "UPDATE keranjang SET qty = qty - $qty_order WHERE seq = $seq_keranjang ";
			$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
						"VALUES (:sql_1, 'F')";
			$query_dml  = $db->prepare($sql);
			$query_dml->bindParam(':sql_1', $sql_1);	
			$query_dml->execute();         
		} 	
//juli tutup jadi tidak jadi dihapus soalnya jika sudah habis lalu datya keep dihapus jadinya gagal balikin datanya, dihapusnya dari hapus otomatis
		//Delete dari keranjang jika sudah diorder
//		$sql =  "DELETE FROM keranjang WHERE customer_seq = :customer_seq  AND user_id = :user_id AND is_ceklis = 'T' AND qty_order > 0 AND qty = 0 ";	
//		$query3 = $db->prepare($sql);
//		$query3->bindParam(':customer_seq', $dataPost['customer_seq']);
//		$query3->bindParam(':user_id', $dataPost['user_id']);
//		$result = $query3->execute();	

//		//Save ke dml
//		$sql_1 = "DELETE FROM keranjang WHERE customer_seq = $customer_seq  AND user_id = '$user_id' AND qty = 0 ";	
//		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
//				 "VALUES (:sql, 'F')";
//		$query_dml2  = $db->prepare($sql);       
//		$query_dml2->bindParam(':sql', $sql_1);         
//		$query_dml2->execute();          
				
		// $sql = "SELECT seq FROM keranjang WHERE customer_seq = :customer_seq AND barang_seq = :barang_seq  AND user_id = :user_id";
		// $querySelect = $db->prepare($sql);
		// $querySelect->bindParam(':customer_seq', $dataPost['customer_seq']);
		// $querySelect->bindParam(':barang_seq', $dataPost['barang_seq']);
		// $querySelect->bindParam(':user_id', $dataPost['user_id']);
		// $result = $querySelect->execute(); 
		// $rowCount = $querySelect->rowCount();

		// if ($rowCount > 0) {
		// 	$sql = "DELETE FROM keranjang_master WHERE customer_seq = :customer_seq  AND user_id = :user_id";
		// 	$query4 = $db->prepare($sql);
		// 	$query4->bindParam(':customer_seq', $dataPost['customer_seq']);
		// 	$query4->bindParam(':user_id', $dataPost['user_id']);	
		// 	$result = $query4->execute();
		// }

        $sql = "DELETE FROM keranjang_master WHERE customer_seq = :customer_seq AND user_id = :user_id";
        $query = $db->prepare($sql);
        $query->bindParam(':customer_seq', $dataPost['customer_seq']);        
        $query->bindParam(':user_id', $dataPost['user_id']);  
		$query->execute(); 

		saveMasterKeranjang($db, $dataPost);

		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
	}		
  	return $response->withJson(["status" => "success", "seq" => $id], 200);   
})->add($cekAPIKey);

//Untuk menghapus keep pesanan 
$app->post('/pesanan_master/delete_keep_pesanan', function (Request $request, Response $response) {
	$db   = $this->db;
	try {
		$db->beginTransaction();
		$dataPost = $request->getParsedBody();

		$query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}
				}
			}
		}

		$tipe_apps = $dataPost['tipe_apps'];

		//Save ke dml
		$sql = 	"SELECT m.seq, m.user_id, d.barang_seq, d.qty, m.customer_seq FROM pesanan_master m, pesanan_detail d ".
				"WHERE m.seq = d.master_seq and m.seq = :seq ";
		$query = $db->prepare($sql);
		$query->bindParam(':seq', $dataPost['seq']);	
		$query->execute();
		$array = $query->fetchAll();

		foreach($array as $row) {      
			$barang_seq   = $row['barang_seq'];
			$qty    	  = $row['qty'];             
			$user_id      = $row['user_id']; 
			$customer_seq = $row['customer_seq']; 			

			$sql = "SELECT seq FROM keranjang WHERE customer_seq = $customer_seq AND barang_seq = $barang_seq AND user_id = '$user_id' ";
			$querySelect = $db->prepare($sql);
			$querySelect->execute(); 
			$rowCount = $querySelect->rowCount(); 

			if ($rowCount > 0) {
				$sql_1 = "UPDATE keranjang SET qty = qty + $qty ".
					 "WHERE barang_seq = $barang_seq and user_id = :user_id";

				$query  = $db->prepare($sql_1); 
				$query->bindParam(':user_id', $user_id);	
				$query->execute();                          

				$sql_1 = "UPDATE keranjang SET qty = qty + $qty ".
						"WHERE barang_seq = $barang_seq and user_id = '$user_id'";
				
						$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
							"VALUES (:sql_1, 'F')";
				$query_dml  = $db->prepare($sql);
				$query_dml->bindParam(':sql_1', $sql_1);	
				$query_dml->execute();                          
			}else{
				$tipe_customer = "";
				$sql =  "INSERT INTO keranjang (customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi, qty_order, user_id, is_ceklis, tgl_input, diskon) ".
                		"VALUES (:customer_seq, :barang_seq, :qty, :tipe_customer, '$tipe_apps', 0, :user_id, 'T', NOW(), 0)";
				$query = $db->prepare($sql);
				$query->bindParam(':customer_seq', $customer_seq);
				$query->bindParam(':barang_seq', $barang_seq);
				$query->bindParam(':qty', $qty);				
				$query->bindParam(':tipe_customer', $tipe_customer);
				$query->bindParam(':user_id', $user_id);				
				$query->execute();

				//get last seq untuk ke dml
				$id = 0;
				$sql = "SELECT MAX(seq) AS seq, DATE_FORMAT(tgl_input, '%d/%m/%Y %H:%i:%s') as tgl_input FROM keranjang WHERE customer_seq = :customer_seq AND user_id = :user_id AND barang_seq = :barang_seq ";
				$querySelect = $db->prepare($sql);
				$querySelect->bindParam(':customer_seq', $customer_seq);  
				$querySelect->bindParam(':user_id', $user_id);  
				$querySelect->bindParam(':barang_seq', $barang_seq);
				$result = $querySelect->execute();
				if ($result) {
					if ($querySelect->rowCount()) {
						$data = $querySelect->fetch();                
						$id = $data["seq"];        
						$tgl_input = $data["tgl_input"];        
					}
				}
				//Save ke dml
				$sql_1 =  "INSERT INTO keranjang (seq, customer_seq, barang_seq, qty, tipe_customer, tipe_aplikasi, qty_order, user_id, is_ceklis, tgl_input) ".
						  "VALUES ($id, $customer_seq, $barang_seq, $qty, '', '$tipe_apps', $qty, '$user_id', 'T', TO_TIMESTAMP('$tgl_input', 'DD/MM/YYYY HH24:MI:SS'))";
		
				$sql =  "INSERT INTO dml_dump (sql_1, is_execute) ".
						"VALUES (:sql_1, 'F')";
				$query1  = $db->prepare($sql);     
				$query1->bindParam(':sql_1', $sql_1);      
				$query1->execute(); 
			}			
		}

		$seq = $dataPost['seq'];

		$sql_1 =	"delete from pesanan_detail where master_seq = $seq";
		$query = $db->prepare($sql_1);
		$result = $query->execute();


		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
					"VALUES (:sql_1, 'F')";
		$query_dml  = $db->prepare($sql);
		$query_dml->bindParam(':sql_1', $sql_1);	
		$query_dml->execute();  

		$sql_1 = "delete from pesanan_master where seq = $seq";
		$query = $db->prepare($sql_1);
		$result = $query->execute();

		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
					"VALUES (:sql_1, 'F')";
		$query_dml  = $db->prepare($sql);
		$query_dml->bindParam(':sql_1', $sql_1);	
		$query_dml->execute();  

		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
	}		
  	return $response->withJson(["status" => "success", "seq" => $id], 200);   
})->add($cekAPIKey);

//Untuk merubah keep pesanan menjadi pesanan
$app->post('/pesanan_master/commit_keep_pesanan', function (Request $request, Response $response) {
	$db   = $this->db;
	try {
		$db->beginTransaction();
		$dataPost = $request->getParsedBody();
		$seq = $dataPost['seq'];

		$query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");			
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}					
				}						
			}
		}

		$no_resi = "";
		$sql = 	"SELECT c.cust_induk_seq, m.jenis_so, m.ekspedisi_seq, IFNULL(m.no_resi,'') AS no_resi ".
				"FROM master_customer c, pesanan_master m where c.seq = m.customer_seq and m.seq = $seq";
		$query = $db->prepare($sql);
		$result = $query->execute();
		$cust_induk_seq = 0;
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["cust_induk_seq"])){
					$cust_induk_seq = $data["cust_induk_seq"];					
				}	
				$jenis_so		= $data["jenis_so"];
				$ekspedisi_Seq	= $data["ekspedisi_seq"];
				$no_resi       	= $data["no_resi"];				
			}
		}
		
		if ($no_resi == ""){
			$sql = "SELECT MIN(seq), no_resi ".
				  "FROM setting_no_resi ".
				  "WHERE tipe = 'A' AND tgl_hapus IS NULL AND is_pakai = 'F' AND ekspedisi_seq = $ekspedisi_Seq";
			$query 	= $db->prepare($sql);
			$result = $query->execute();
			if ($result) {
				if ($query->rowCount()) {
					$data = $query->fetch();
					if (!empty($data["no_resi"])){
						$no_resi = $data["no_resi"];					
					}	
				}
			}
		}
		
		$sqlResi = "";
		if ($no_resi != ""){
			$sqlResi = ", no_resi = '$no_resi'";
		}	

		if ($cust_induk_seq == 0){
			$sql_1 = "UPDATE pesanan_master SET status = 'B' $sqlResi WHERE seq = $seq";
		}else{
			$sql_1 = "UPDATE pesanan_master SET status = 'E' $sqlResi WHERE seq = $seq";	
		}		
		
		$query = $db->prepare($sql_1);
		$result = $query->execute();

		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
				 "VALUES (:sql_1, 'F')";
		$query_dml  = $db->prepare($sql);
		$query_dml->bindParam(':sql_1', $sql_1);	
		$query_dml->execute();
		
		if (($cust_induk_seq == 0) && ($jenis_so != "P")) {			
			$sql = "Insert into histori_approve (tanggal, trans_seq) values(now(), $seq)";						
			$query = $db->prepare($sql);
			$result = $query->execute();
		}		

		$sql_1 = "UPDATE setting_no_resi SET is_pakai = 'T' WHERE no_resi = '$no_resi'";
		$query = $db->prepare($sql_1);
		$result = $query->execute();

		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
				 "VALUES (:sql_1, 'F')";
		$query_dml  = $db->prepare($sql);
		$query_dml->bindParam(':sql_1', $sql_1);	
		$query_dml->execute();

		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
	}		
  	return $response->withJson(["status" => "success", "seq" => $id], 200);   
})->add($cekAPIKey);


//Untuk approve pesanan customer sub agen
$app->post('/pesanan_master/approve', function (Request $request, Response $response) {
	$db   = $this->db;
	try {
		$db->beginTransaction();
		$dataPost = $request->getParsedBody();
		$seq 	  = $dataPost['seq'];

		$query = $db->prepare("SELECT is_on FROM setting_on_off WHERE seq IN ( SELECT MAX(seq) FROM setting_on_off )");			
		$result = $query->execute();
		if ($result) {
			if ($query->rowCount()) {
				$data = $query->fetch();
				if (!empty($data["is_on"])){
					if ($data["is_on"] == 'F'){
						return $response->withJson(["status" => "status off", "seq" => 0], 200);
					}					
				}						
			}
		}

		$sql = "SELECT status FROM pesanan_master WHERE seq = $seq";
		$query = $db->prepare($sql);			
		$query->execute();				
		$data = $query->fetch();
		if ($data["status"] != "E"){
			return $response->withJson(["status" => "sudah approve"], 200);
		}

		$sql_1 = "UPDATE pesanan_master set status = 'P' WHERE seq = $seq";
		$query = $db->prepare($sql_1);
		$query->execute();

		$sql   = "INSERT INTO dml_dump (sql_1, is_execute) ".
				 "VALUES (:sql_1, 'F')";
		$query_dml  = $db->prepare($sql);
		$query_dml->bindParam(':sql_1', $sql_1);	
		$query_dml->execute();  

		$sql = "INSERT INTO histori_approve (tanggal, trans_seq) VALUES (now(), $seq)";
		$query = $db->prepare($sql);
		$query->execute();

		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();
		throw $pdoe;
		return $response->withJson(["status" => "gagal"], 100);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(["status" => "gagal"], 100);  
	}		
  	return $response->withJson(["status" => "success"], 200);
})->add($cekAPIKey);

//Untuk mendapatkan data pesanan dan detailnya
$app->get('/pesanan_master/get/{id}', function (Request $request, Response $response, array $args) {

	$sql =	"SELECT DATE_FORMAT(m.tanggal, '%d-%m-%Y') as tanggal, m.nomor, m.customer_seq, m.total as totalmst, m.status, b.seq as brgseq, b.barcode, b.nama as nama, ".
			"b.barcode as kode, b.gambar, d.harga as harga, d.seq as seq, d.barang_seq as brg_seq,  d.qty as qty, d.total as totalDet, d.diskon as diskon, m.diskon as diskonmst, ".
			"m.subtotal as subtotalmst, b.gambar_sedang as gambar_besar, m.alamat_kirim, m.alamat_pengirim, b.keterangan as keterangan_barang, ".
			"m.user_id, m.total_berat, m.jenis_so, m.is_dropship, m.no_resi, m.nama_pengirim, m.alamat_pengirim_lengkap, m.no_telepon_pengirim, m.nama_kirim, m.id_provinsi, ".
			"m.id_kota, m.id_kecamatan, m.kelurahan, m.provinsi, m.kota, m.kecamatan, m.alamat_kirim_lengkap, m.no_telepon_kirim, m.ongkos_kirim, m.ekspedisi, m.ekspedisi_seq, m.service, m.is_selesai_chat, (d.berat * d.qty) as berat_total ".
			"FROM pesanan_master m, pesanan_detail d, master_barang b ".
			"WHERE m.seq = d.master_seq and b.seq = d.barang_seq and m.seq = :id";
	$query = $this->db->prepare($sql);
  	$query->bindParam(':id', $args['id']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});


//Untuk mendapatkan data pesanan
$app->get('/pesanan_master/get_master/{id}', function (Request $request, Response $response, array $args) {

	$sql =	"SELECT DATE_FORMAT(m.tanggal, '%d-%m-%Y') as tanggal, m.nomor, m.customer_seq, m.total as totalmst, m.status, m.diskon as diskonmst, m.subtotal as subtotalmst, m.alamat_kirim, ".
			"m.alamat_pengirim, m.user_id, m.total_berat, m.jenis_so, m.is_dropship, m.no_resi, m.nama_pengirim, m.alamat_pengirim_lengkap, m.no_telepon_pengirim, m.nama_kirim, m.id_provinsi, ".
			"m.id_kota, m.id_kecamatan, m.kelurahan, m.provinsi, m.kota, m.kecamatan, m.alamat_kirim_lengkap, m.no_telepon_kirim, m.ongkos_kirim, m.ekspedisi, m.ekspedisi_seq, m.service, m.is_selesai_chat ".
			"FROM pesanan_master m ".
			"WHERE m.seq = :id";
	$query = $this->db->prepare($sql);
  	$query->bindParam(':id', $args['id']);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(
				'kode' => 200,
				'keterangan' => 'Tidak ada data',
				'data' => null);
		}
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});

//Untuk mendapatkan daftar pesanan
$app->get('/pesanan_master/load', function (Request $request, Response $response, array $args) {
	$customer_seq = $request->getQueryParam("customer_seq");	
	$offset       = $request->getQueryParam("offset");
	$status       = $request->getQueryParam("status");
	//+new
	$tgl_dari     = $request->getQueryParam("tgl_dari");
	$tgl_sampai   = $request->getQueryParam("tgl_sampai");
	$user_id      = $request->getQueryParam("user_id");
	//--
	
	$filter = "";
	if (!empty($status) && ($status != "")){
		$filter .= " AND m.status = '$status'";
	}

	//+new
	if (!empty($tgl_dari) && ($tgl_dari != "")){
		$filter .= " AND DATE(m.tanggal) >= DATE('$tgl_dari') ";
	}

	if (!empty($tgl_sampai) && ($tgl_sampai != "")){
		$filter .= " AND DATE(m.tanggal) <= DATE('$tgl_sampai') ";
	}
	//--

	$sql = 	"SELECT m.seq as seq, DATE_FORMAT(m.tanggal, '%d-%m-%Y') as tanggal, m.nomor, m.customer_seq, m.total as total, m.diskon, m.subtotal, m.status, m.alamat_kirim, m.alamat_pengirim, ".
			"m.user_id, m.total_berat, m.jenis_so, m.is_dropship, m.no_resi, m.nama_pengirim, m.alamat_pengirim_lengkap, m.no_telepon_pengirim, m.nama_kirim, m.id_provinsi, ".
			"m.id_kota, m.id_kecamatan, m.kelurahan, m.provinsi, m.kota, m.kecamatan, m.alamat_kirim_lengkap, m.no_telepon_kirim, m.ongkos_kirim, m.ekspedisi, m.ekspedisi_seq, m.service, m.is_selesai_chat,".
			"(SELECT COUNT(*) FROM chat_box c WHERE c.is_baca = 'F' AND c.tipe_pesan = 'D' AND c.pesanan_seq = m.seq AND c.isi_pesan <> '') AS notif ".
			"FROM pesanan_master m ".
			"WHERE m.customer_seq = $customer_seq AND m.user_id = '$user_id' $filter order by seq desc limit 10  OFFSET $offset ";		
	$query = $this->db->prepare($sql);	
	  
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});

//Untuk mendapatkan daftar pesanan sub agen
$app->get('/pesanan_master/load_pesanan_sub_agen', function (Request $request, Response $response, array $args) {
	$search       = $request->getQueryParam("search");
	$customer_seq = $request->getQueryParam("customer_seq");
	$sub_agen_seq = $request->getQueryParam("sub_agen_seq");	
	$offset       = $request->getQueryParam("offset");
	$status       = $request->getQueryParam("status");
	$tgl_dari     = $request->getQueryParam("tgl_dari");
	$tgl_sampai   = $request->getQueryParam("tgl_sampai");	

	$filter = "";
	if (!empty($status) && ($status != "")){
		$filter .= " AND m.status = '$status'";
	}		

	if (!empty($search)){
		$filter .= " AND ((c.nama LIKE '%$search%') OR (m.nomor LIKE '%$search%') OR (m.tanggal LIKE '%$search%') OR (m.subtotal LIKE '%$search%') OR (m.total LIKE '%$search%') OR (m.diskon LIKE '%$search%'))";
	}	

	if (!empty($tgl_dari) && ($tgl_dari != "")){
		$filter .= " AND DATE(m.tanggal) >= DATE('$tgl_dari') ";
	}

	if (!empty($tgl_sampai) && ($tgl_sampai != "")){
		$filter .= " AND DATE(m.tanggal) <= DATE('$tgl_sampai') ";
	}

	if (!empty($sub_agen_seq) && ($sub_agen_seq != "")){
		$filter .= " AND c.seq = $sub_agen_seq ";
	}		

	$sql = 	"SELECT m.seq as seq, DATE_FORMAT(m.tanggal, '%d-%m-%Y') as tanggal, m.nomor, m.customer_seq, m.total as total, m.diskon, m.subtotal, m.status, m.alamat_kirim, m.alamat_pengirim, ".
			"m.user_id, m.total_berat, m.jenis_so, m.is_dropship, m.no_resi, m.nama_pengirim, m.alamat_pengirim_lengkap, m.no_telepon_pengirim, m.nama_kirim, m.id_provinsi, ".
			"m.id_kota, m.id_kecamatan, m.kelurahan, m.provinsi, m.kota, m.kecamatan, m.alamat_kirim_lengkap, m.no_telepon_kirim, m.ongkos_kirim, m.ekspedisi, m.ekspedisi_seq, m.service, m.is_selesai_chat, c.nama AS nama_customer ".			
			"FROM pesanan_master m, master_customer c ".
			"WHERE m.customer_seq = c.seq AND c.cust_induk_seq = $customer_seq $filter ORDER BY seq DESC limit 10  OFFSET $offset ";		
	$query = $this->db->prepare($sql);		  
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['seq' => 0]);
		}
	}else{
			$data = array(['seq' => 0]);
	}
  return $response->withJson($data);
});

$app->get('/pesanan_master/load_sink_desktop', function (Request $request, Response $response, array $args) {	 	
	$seq = $request->getQueryParam("seq"); 	
	$sql =  "SELECT m.seq as seq, m.tanggal as tanggal, m.nomor, m.customer_seq, m.total as total, m.status, m.tipe_customer, m.diskon_ethica, m.diskon_seply, m.diskon, ".
			"m.subtotal, m.alamat_kirim, m.alamat_pengirim, m.user_id, m.total_berat, m.jenis_so, m.is_dropship, m.no_resi, m.nama_pengirim, m.alamat_pengirim_lengkap, m.no_telepon_pengirim, ".
			"m.nama_kirim, m.id_provinsi, m.id_kota, m.id_kecamatan, m.kelurahan, m.provinsi, m.kota, m.kecamatan, m.alamat_kirim_lengkap, m.no_telepon_kirim, m.ongkos_kirim, m.ekspedisi, m.ekspedisi_seq, m.service, m.is_selesai_chat, m.diskon_ethica_hijab ".
		   "FROM pesanan_master m WHERE m.seq > $seq";	
  	$query = $this->db->prepare($sql);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array();
		}
	}else{
			$data = array();
	}
  	return $response->withJson($data);
});

$app->get('/pesanan_detail/load_sink_desktop', function (Request $request, Response $response, array $args) {	 	
	$seq = $request->getQueryParam("seq"); 	
	$sql = "SELECT seq, master_seq, barang_seq, qty, harga, total, diskon, subtotal, berat ".
		   "FROM pesanan_detail WHERE master_seq > $seq ";
  	$query = $this->db->prepare($sql);
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array();
		}
	}else{
			$data = array();
	}
  return $response->withJson($data);
});


$app->post('/pesanan_master/update_kirim', function (Request $request, Response $response, array $args) {
  $dataPost = $request->getParsedBody();
  $seq   = $dataPost['seq'];  
  $query = $this->db->prepare("UPDATE pesanan_master SET is_kirim = 'T' where seq in($seq)");
	$result = $query->execute();
	if ($result) {
		$data = array("status" => "success", 
									"seq" => $id);
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});


$app->post('/pesanan_detail/update_kirim', function (Request $request, Response $response, array $args) {
  $dataPost = $request->getParsedBody();
  $seq   = $dataPost['seq'];  

  $query = $this->db->prepare("UPDATE pesanan_detail SET is_kirim = 'T' where seq in($seq)");
	$result = $query->execute();
	if ($result) {
		$data = array("status" => "success", 
									"seq" => $id);
	}else{
		$data = array(
			'kode' => 100,
			'keterangan' => 'Terdapat error',
			'data' => null);
	}
  return $response->withJson($data);
});

//Untuk mendapatkan histori alamat pengiriman
$app->get('/pesanan_master/load_histori_alamat_kirim', function (Request $request, Response $response, array $args) {
	$search       = $request->getQueryParam("search");
	$customer_seq = $request->getQueryParam("customer_seq");	
	$user_id      = $request->getQueryParam("user_id");		
	$offset       = $request->getQueryParam("offset");	

	$search = str_replace("'", "''", $search);

	$filter = "";
	if (!empty($search)){
		$filter .= " AND alamat_kirim LIKE '%$search%'";	
	}
	
	$alamat_lengkap = "CONCAT('Kepada\n', 'Yth : ', nama_kirim, '\nAlamat : ', alamat_kirim_lengkap, ', ' , kelurahan, ', ' ,kecamatan, ', ' ,kota, ', ' ,provinsi, '\nNo. HP : ', no_telepon_kirim)";

	$sql = 	"SELECT DISTINCT nama_kirim, kelurahan, alamat_kirim_lengkap, provinsi, kota, kecamatan, $alamat_lengkap AS alamat_kirim, no_telepon_kirim, id_provinsi, id_kota, id_kecamatan ".
			"FROM pesanan_master ".
			"WHERE customer_seq = $customer_seq AND user_id = '$user_id' AND is_data_lama = 'F' $filter ".
			"ORDER BY seq DESC limit 15  OFFSET $offset ";
	$query = $this->db->prepare($sql);
	  
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
			$data = $query->fetchAll();
		}else{
			$data = array(['alamat_kirim' => ""]);
		}
	}else{
			$data = array(['alamat_kirim' => ""]);
	}
  return $response->withJson($data);
});

//Untuk mendapatkan daftar pesanan sub agen
$app->get('/pesanan_master/get_count_pesanan_sub_agen', function (Request $request, Response $response, array $args) {	
	$customer_seq = $request->getQueryParam("customer_seq");	
	$status       = $request->getQueryParam("status");

	$filter = "";

	if($status != ""){
		$filter .= "AND status = '$status' ";
	}

	$sql =  "SELECT COUNT(*) AS jumlah FROM pesanan_master ".
			"WHERE customer_seq IN ( SELECT seq FROM master_customer WHERE cust_induk_seq = $customer_seq ) ".$filter;			
	$query = $this->db->prepare($sql);        
	$result = $query->execute();
	if ($result) {
		if ($query->rowCount()) {
		$data = $query->fetchAll();
		}else{
		$data = array(
			'kode' => 200,
			'keterangan' => 'Tidak ada data',
			'data' => null);
		}
	}else{
		$data = array(
		'kode' => 100,
		'keterangan' => 'Terdapat error',
		'data' => null);
	}
	return $response->withJson($data);
});




//------------------------------------------------------------ API LAMA ------------------------------------------------------------\\
// $app->get('/pesanan_master/load_old', function (Request $request, Response $response, array $args) {

// 	$customer_seq = $request->getQueryParam("customer_seq");
// 	$tipe_cust    = $request->getQueryParam("tipe_cust");
// 	$offset       = $request->getQueryParam("offset");
// 	$status       = $request->getQueryParam("status");	
	
// 	$filter = "";
// 	if (!empty($status) && ($status != "")){
// 		$filter .= " AND m.status = '$status'";
// 	}
	
//   $query = $this->db->prepare("SELECT m.seq as seq, DATE_FORMAT(m.tanggal, '%d-%m-%Y') as tanggal, m.nomor, m.customer_seq, m.total as total, m.diskon, m.subtotal, m.status, m.alamat_kirim, m.alamat_pengirim  
// 															FROM pesanan_master m 
// 															where m.customer_seq = $customer_seq and m.tipe_customer = '$tipe_cust' $filter order by seq desc limit 10  OFFSET $offset ");
// 	$result = $query->execute();
// 	if ($result) {
// 		if ($query->rowCount()) {
// 			$data = $query->fetchAll();
// 		}else{
// 			$data = array(['seq' => 0]);
// 		}
// 	}else{
// 			$data = array(['seq' => 0]);
// 	}
//   return $response->withJson($data);
// });


// $app->get('/pesanan_master/load_sink_desktop', function (Request $request, Response $response, array $args) {	 	
// 	$sql = "SELECT m.seq as seq, m.tanggal as tanggal, m.nomor, m.customer_seq, m.total as total, m.status, m.tipe_customer, m.diskon_ethica, m.diskon_seply, m.diskon, m.subtotal, m.alamat_kirim, m.alamat_pengirim ".
// 		   "FROM pesanan_master m where is_kirim = 'F'";
//   	$query = $this->db->prepare($sql);
// 	$result = $query->execute();
// 	if ($result) {
// 		if ($query->rowCount()) {
// 			$data = $query->fetchAll();
// 		}else{
// 			$data = array();
// 		}
// 	}else{
// 			$data = array();
// 	}
//   	return $response->withJson($data);
// });

// $app->get('/pesanan_detail/load_sink_desktop', function (Request $request, Response $response, array $args) {	 	
// 	$sql = "SELECT seq, master_seq, barang_seq, qty, harga, total, diskon, subtotal, berat FROM pesanan_detail where is_kirim = 'F' ";
//   	$query = $this->db->prepare($sql);
// 	$result = $query->execute();
// 	if ($result) {
// 		if ($query->rowCount()) {
// 			$data = $query->fetchAll();
// 		}else{
// 			$data = array();
// 		}
// 	}else{
// 			$data = array();
// 	}
//   return $response->withJson($data);
// });


