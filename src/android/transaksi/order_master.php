<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/keranjang/save', function (Request $request, Response $response) {

  $dataPost = $request->getParsedBody();

  $query = $this->db->prepare('insert into keranjang (seq, customer_seq, barang_seq, qty) values 
  	((select max(seq)+1 from keranjang), :customer_seq, :barang_seq, :qty)');
  $query->bindParam(':customer_seq', $dataPost['customer_seq']);
  $query->bindParam(':barang_seq', $dataPost['barang_seq']);
  $query->bindParam(':qty', $dataPost['qty']);
	$result = $query->execute();
	if($result->execute())
   	return $response->withJson(["status" => "success", "data" => "1"], 200);   
})->add($cekAPIKey);
