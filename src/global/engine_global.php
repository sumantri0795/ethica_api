<?php

function getPlafonTerpakai($db, $customer_seq, $barang_seq, $user_id, $jml_input, $ongkos_kirim, $field_qty){    
    $totalKeranjang = get_nilai_keranjang($db, $customer_seq, $user_id, $barang_seq, $jml_input, $field_qty);
    
    $sql =  "SELECT customer_seq, SUM(jumlah) AS jumlah FROM ( ".
                "SELECT customer_seq, jumlah AS jumlah ".
                "FROM penambah_pengurang_plafon ".
                "WHERE customer_seq = $customer_seq ".
                " UNION ALL ".
                "SELECT customer_seq, total AS jumlah ".                    
                "FROM pesanan_master ".
                "WHERE customer_seq = $customer_seq AND status NOT IN ('T','H') AND is_data_lama = 'F' ".
        ") AS hasil GROUP BY customer_seq ";      
    $qry = $db->prepare($sql);  
    $qry->execute(); 
    $rowCount = $qry->rowCount();       
    $terpakai = 0;
    if($rowCount > 0){
        $hasil = $qry->fetch();
        $terpakai  = $hasil["jumlah"];      
    }
  
  
    $sql =  "SELECT plafon FROM master_customer WHERE seq = $customer_seq";
    $query = $db->prepare($sql);    
    $query->execute();
    $data = $query->fetch();               
    
    $plafond = $data["plafon"];  
    return ($plafond == 0)||(($totalKeranjang + $terpakai + $ongkos_kirim) <= $plafond);
}


function get_nilai_keranjang($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty){  
  
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty) AS qty FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty ".
              "FROM keranjang WHERE customer_seq = $customer_seq ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input  AS qty ".              
            ") AS barang GROUP BY barang_seq ";
    $qry1 = $db->prepare($sql);
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));
  
    //Ambil pct diskon di detail customer ------------------------------------------
    $diskon_pct = 0;  
    $sql =  "SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab ".
            "FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id'";
    $qry = $db->prepare($sql); 
    $qry->execute();     
    
    $hasil = $qry->fetch();    
    $pct_diskon_ethica       = $hasil["pct_diskon_ethica"];
    $pct_diskon_seply        = $hasil["pct_diskon_seply"];
    $pct_diskon_ethica_hijab = $hasil["pct_diskon_ethica_hijab"];      
  
    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        if ($barang_seq > 0) {
            $qty        = $row['qty'];      
    
            //Ambil barang & brand ----------------------------------------------------------
            $sql =  "SELECT b.harga, b.brand_seq, b.sub_brand_seq, d.is_ethica, d.is_seply, d.is_ethicahijab ".
                    "FROM master_barang b, master_brand d ".
                    "WHERE b.brand_seq = d.seq AND b.seq = $barang_seq ";
            $qry = $db->prepare($sql);    
            $qry->execute(); 
            $hasil = $qry->fetch();
            $harga           = $hasil["harga"];
            $is_ethica       = $hasil["is_ethica"];
            $is_seply        = $hasil["is_seply"];
            $is_ethica_hijab = $hasil["is_ethicahijab"];  
            
            //-------------------------------------------------------------------------------
            $disk_ethica = 0;  $disk_seply = 0; $disk_ethica_hijab = 0;
    
            if ($is_ethica == 'T'){
                $disk_ethica = $pct_diskon_ethica;
            }
    
            if ($is_seply == 'T'){
                $disk_seply = $pct_diskon_seply;
            }
    
            if ($is_ethica_hijab == 'T'){
                $disk_ethica_hijab = $pct_diskon_ethica_hijab;
            }    
            
            //cari diskon tertinggi
            $diskon_pct = MAX($disk_ethica, $disk_seply, $disk_ethica_hijab);
    
            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND ((s.brand_seq = b.brand_Seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = b.klasifikasi) ".					
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
        
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                    }
                }
            } 
  
            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }
            $harga = $harga - (($harga / 100) * $diskonAkhir);

            $Total += $harga * $qty;
        }
    }   
    return $Total;
  }


function get_nilai_diskon($db, $customer_seq, $user_id, $brg_input_seq, $jml_input, $field_qty, &$from_diskon, &$maxDiskonSetting){  
    $from_diskon = "F";
    $maxDiskonSetting = 0;
    //sql nyari barang dan qty di keranjang dan disatun dengan barang yang diinput
    $sql =  "SELECT barang_seq, SUM(qty_order) AS qty_order FROM ( ".
              "SELECT barang_seq AS barang_seq, $field_qty AS qty_order ".
              "FROM keranjang WHERE customer_seq = $customer_seq AND barang_seq = $brg_input_seq AND user_id = '$user_id' ".
                "UNION ALL ".
              "SELECT DISTINCT $brg_input_seq AS barang_seq, $jml_input AS qty_order ".              
            ") AS barang GROUP BY barang_seq ";            
    $qry1 = $db->prepare($sql);        
    $qry1->execute();
    $array = $qry1->fetchAll();
    //die(var_dump($array));
  
    //Ambil pct diskon di detail customer ------------------------------------------
    $diskon_pct = 0;  
    $sql =  "SELECT pct_diskon_ethica, pct_diskon_seply, pct_diskon_ethica_hijab ".
            "FROM detail_customer WHERE master_seq = $customer_seq AND user_id = '$user_id'";
    $qry = $db->prepare($sql); 
    $qry->execute();     
    
    $hasil = $qry->fetch();    
    $pct_diskon_ethica       = $hasil["pct_diskon_ethica"];
    $pct_diskon_seply        = $hasil["pct_diskon_seply"];
    $pct_diskon_ethica_hijab = $hasil["pct_diskon_ethica_hijab"];      
  
    $Total = 0;
    foreach($array as $row) { 
        $barang_seq = $row['barang_seq']; 
        $qty        = $row['qty_order'];
        if (($barang_seq > 0) && ($qty > 0)){            

            //Ambil barang & brand ----------------------------------------------------------
            $sql = "SELECT b.harga, b.brand_seq, b.sub_brand_seq, d.is_ethica, d.is_seply, d.is_ethicahijab ".
                    "FROM master_barang b, master_brand d ".
                    "WHERE b.brand_seq = d.seq AND b.seq = $barang_seq ";
            $qry = $db->prepare($sql);    
            $qry->execute(); 
            $hasil = $qry->fetch();
            $is_ethica       = $hasil["is_ethica"];
            $is_seply        = $hasil["is_seply"];
            $is_ethica_hijab = $hasil["is_ethicahijab"];  
            
            //-------------------------------------------------------------------------------
            $disk_ethica = 0;  $disk_seply = 0; $disk_ethica_hijab = 0;
        
            if ($is_ethica == 'T'){
                $disk_ethica = $pct_diskon_ethica;
            }
        
            if ($is_seply == 'T'){
                $disk_seply = $pct_diskon_seply;
            }
        
            if ($is_ethica_hijab == 'T'){
                $disk_ethica_hijab = $pct_diskon_ethica_hijab;
            }    
            
            //cari diskon tertinggi
            $diskon_pct = MAX($disk_ethica, $disk_seply, $disk_ethica_hijab);
        
        
            //Cek apakah ada setting diskon atau tidak ------------------------------------
            $sql =  "SELECT range_mulai, range_sampai, diskon_pct ".
                    "FROM setting_diskon_master sm, setting_diskon_detail s, master_barang b ".
                    "WHERE sm.seq = s.master_seq AND DATE(DATE_FORMAT(NOW(),'%y-%m-%d')) BETWEEN DATE(sm.tgl_berlaku_dari) AND DATE(sm.tgl_berlaku_sampai) ".
                    "AND ((s.brand_seq = b.brand_Seq AND s.sub_brand_seq = b.sub_brand_seq AND s.klasifik = b.klasifikasi) ".					
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // semua 
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND s.klasifik = 'Semua') ". // sub brand dan kelasifikai semua 
                            "or (b.brand_seq = s.brand_seq AND b.sub_brand_seq = s.sub_brand_seq AND s.klasifik = 'Semua') ". // klasifikasi semua
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // brand dan sub brand semua 
                            "or (s.brand_seq = 0 AND s.sub_brand_seq = b.sub_brand_seq AND b.klasifikasi = s.klasifik) ". // brand sama
                            "or (b.brand_seq = s.brand_seq AND s.sub_brand_seq = 0 AND b.klasifikasi = s.klasifik) ". // sub brand semua 
                    ") AND sm.tgl_hapus IS NULL AND b.seq = $barang_seq AND b.is_preorder <> 'T' ";

            $qry = $db->prepare($sql); 
            $qry->execute(); 
            $rowCountSetting = $qry->rowCount(); 
            $diskon_pct_setting = 0;
            if ($rowCountSetting > 0){
                $arraySetDiskon = $qry->fetchAll();               
                foreach($arraySetDiskon as $rowSet) { 
                    $dari   = $rowSet['range_mulai'];         
                    $sampai = $rowSet['range_sampai']; 
            
                    if (($qty >= $dari) && ($qty <= $sampai)){
                        $diskon_pct_setting = $rowSet['diskon_pct'];
                        $from_diskon = "T";
                    }
                    $maxDiskonSetting = $diskon_pct_setting;
                }
            } 
    
            //jika ada diskon dari setting maka yang diambil diskon dari setting
            $diskonAkhir = 0;
            if ($diskon_pct_setting > 0){
                $diskonAkhir = $diskon_pct_setting;
            }else{
                $diskonAkhir = $diskon_pct;
            }
            $Total = $diskonAkhir;
        }
    } 
    return $Total; 
}
  
function getStokBarang($db, $barang_seq, $keranjangseq){    
    $sql =  "SELECT barang_seq, SUM(stok) AS stok FROM (".
                "SELECT barang_seq, qty AS stok ".
                "FROM penambah_pengurang_stok WHERE barang_seq = :barang_seq ".
                
                " UNION ALL ".
                
                "SELECT barang_seq, -qty AS stok ".                    
                "FROM pesanan_detail WHERE barang_seq = :barang_seq ".
                "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND is_data_lama <> 'T')".
                
                " UNION ALL ".
                
                "SELECT barang_seq, -qty AS stok ".                    
                "FROM keranjang WHERE barang_seq = :barang_seq and seq <> $keranjangseq ".                    
        ") AS stoks GROUP BY barang_seq ";            
    $qryCekStok = $db->prepare($sql);  
    $qryCekStok->bindParam(':barang_seq', $barang_seq);  
    $qryCekStok->execute(); 
    $rowCount = $qryCekStok->rowCount(); 
    
    $stok = 0;
    if ($rowCount > 0) {
      $hasil = $qryCekStok->fetch();
      $stok  = $hasil["stok"];      
    }  
    return $stok;
  }
  
  
function getStokBarangPreOrder($db, $barang_seq, $keranjangseq){    
    $sql =  "SELECT barang_seq, SUM(stok) AS stok FROM (".              
                "SELECT seq AS barang_seq, jumlah_pesan AS stok ".
                "FROM master_barang ".
                "WHERE seq = $barang_seq ".
  
                " UNION ALL ".
    
                "SELECT barang_seq, -qty AS stok ".                    
                "FROM pesanan_detail WHERE barang_seq = $barang_seq ".
                "AND master_seq IN (SELECT seq FROM pesanan_master WHERE status NOT IN ('T','H', 'S') AND jenis_so ='P' AND is_data_lama <> 'T') ".
                
                " UNION ALL ".
                
                "SELECT barang_seq, -qty AS stok ".                    
                "FROM keranjang WHERE barang_seq = $barang_seq and seq <> $keranjangseq ".                    
            ") AS stoks GROUP BY barang_seq ";                  
    $qryCekStok = $db->prepare($sql);  
    $qryCekStok->bindParam(':barang_seq', $barang_seq);  
    $qryCekStok->execute(); 
    $rowCount = $qryCekStok->rowCount(); 
    
    $stok = 0;
    if ($rowCount > 0) {
      $hasil = $qryCekStok->fetch();
      $stok  = $hasil["stok"];      
    }  
    return $stok;
}

// function set_lock_table($db, $nama_table){    
//     $sql =  "SELECT COUNT(*) FROM $nama_table LOCK IN SHARE MODE";
//     $qry = $db->prepare($sql);
//$qry->execute();
// }


function set_lock_table($db, $nama_table){
    $sql =  "LOCK TABLE $nama_table WRITE";
    $qry = $db->prepare($sql);
    $qry->execute();
}


function set_unlock_table($db){
    $sql =  "UNLOCK TABLE";
    $qry = $db->prepare($sql);
    $qry->execute();
}

function wait_lock_table($db, $nama_table){
    $sql =  "SELECT count(*) FROM $nama_table";
    $qry = $db->prepare($sql);
    $qry->execute();
}